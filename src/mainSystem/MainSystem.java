package mainSystem;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.UIManager;

import view.CaracFrame;
import view.RefereeFrame;

public class MainSystem {
	
	public final static boolean IN_TESTING = false ;

	public static void newFrame(CaracFrame frame){
		frame.dispose();
		CaracFrame newFrame = new CaracFrame();
		newFrame.setVisible(true);
		newFrame.setResizable(false);
	}

	public static void main(String[] args){
		setBestLookAndFeelAvailable();
		//CaracFrame frame = new CaracFrame();
		//RefereeFrame frame = new RefereeFrame();
		String[] choix = {"Créer sa signature", "Acceder à l'outil arbitre"};
		int rang = -1 ;
		rang = JOptionPane.showOptionDialog(null, 
				"Quel programme voulez-vous executer ?",
				"Selection du programme",
				JOptionPane.YES_NO_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				choix,
				null);
		if(rang == 0){
			CaracFrame frame = new CaracFrame();
			frame.setVisible(true);
			frame.setResizable(false);
		}
		else if(rang == 1){
			String password = "blop";
			JPasswordField pf = new JPasswordField();
			int okCxl = -2;
			while(!new String(pf.getPassword()).toLowerCase().equals(password) && okCxl != JOptionPane.CANCEL_OPTION && okCxl != JOptionPane.CLOSED_OPTION)
			{
				okCxl = JOptionPane.showConfirmDialog(null, pf, "Veuillez entrer le mot de passe", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
			}
			
			if(okCxl == JOptionPane.CANCEL_OPTION){
				main(new String[0]);
			}
			else if(okCxl == JOptionPane.OK_OPTION && new String(pf.getPassword()).toLowerCase().equals(password)) {
				RefereeFrame frame = new RefereeFrame();
				frame.setVisible(true);
				frame.setResizable(false);
			}
		}
		//frame.setResizable(false);
	}

	public static void setBestLookAndFeelAvailable(){
		String system_lf = UIManager.getSystemLookAndFeelClassName().toLowerCase();
		if(system_lf.contains("metal")){
			try {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			}catch (Exception e) {}
		}else{
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			}catch (Exception e) {}
		}
	}
}