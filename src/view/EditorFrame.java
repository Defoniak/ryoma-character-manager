package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import carac.Utilitaires;

import component.PlayerEditorPanel;

public class EditorFrame extends JFrame implements ActionListener{
	
	PlayerEditorPanel panel;
	
	JMenuBar menu = new JMenuBar();

	JMenu fileMenu = new JMenu("Fichier");
	JMenuItem fileRegister = new JMenuItem("Enregistrer la fiche actuelle");
	JMenuItem fileLoader = new JMenuItem("Charger une fiche");
	
	public EditorFrame(PlayerEditorPanel panel){
		super("mode �dition");
		this.panel = panel;
		this.setVisible(true);
		menu.add(fileMenu);
		fileMenu.add(fileRegister);
		fileMenu.add(fileLoader);
		//this.setJMenuBar(menu);
		add(panel);
		
		setSize (900, 300);
		this.setLocationRelativeTo(null);
		pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JMenuItem menuItem = (JMenuItem)e.getSource();
		if(menuItem == fileRegister){
			
		}
		else if(menuItem == fileLoader){
			//SelectPlayerFrame selectPlayerFrame = new SelectPlayerFrame(this,"Selection d'une fiche joueur",true);
			String path = (String)JOptionPane.showInputDialog(null, 
				      "Veuillez choisir une fiche de joueur � charger",
				      "Chargement du joueur",
				      JOptionPane.QUESTION_MESSAGE,
				      null,
				      Utilitaires.getListPlayer(),
				      Utilitaires.getListPlayer()[0]);
			if(! path.equals(null)){
				panel.setPlayer(Utilitaires.loadPlayer("player/" + path));
			}
		}
		
	}
}
