package view;

import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import carac.Fields;
import carac.Utilitaires;

public class PreviousLevel extends JPanel implements ActionListener{
	private ArrayList<JTextField> listJTextField = new ArrayList<JTextField>();
	private ArrayList<JLabel> listJLabel = new ArrayList<JLabel>();
	private int[] valueTab = new int[Fields.NUM_CARAC];
	private int[] previousValue = new int[9];
	private int[] minimumValue = new int[8];
	private JButton button = new JButton("restaurer ce niveau");
	private CaracFrame frame;
	
	public PreviousLevel(CaracFrame frame){
		super();
		this.frame = frame;
		for(int i = 0; i < Fields.NUM_CARAC; i++){
			listJTextField.add(new JTextField("0"));
		}
		for(int i = 0; i < valueTab.length ; i++){
			valueTab[i] = 0;
		}
		for(int i = 0; i <Fields.NUM_CARAC ;i++){
			listJLabel.add(new JLabel(Fields.getCaracName(i)));
		}
		listJTextField.get(Fields.NIVEAU).setText("1");
		listJTextField.get(Fields.HP).setText("100");
		listJTextField.get(Fields.MP).setText("100");
		valueTab[Fields.PR] = 40;
		valueTab[Fields.NIVEAU] = 1;
		valueTab[Fields.HP] = 100;
		valueTab[Fields.MP] = 100;
		display();
		button.addActionListener(this);
	}
	
	public PreviousLevel(int[] valueTab, int[] previousValue, int[] minimumValue, CaracFrame frame){
		super();
		this.frame = frame;
		for(int i = 0; i <Fields.NUM_CARAC ;i++){
			listJLabel.add(new JLabel(Fields.getCaracName(i)));
		}
		for(int i = 0; i < previousValue.length; i++){
			this.previousValue[i] = previousValue[i];
		}
		for(int i = 0; i < minimumValue.length; i++){
			this.minimumValue[i] = minimumValue[i];
		}
		for(int i = 0; i < valueTab.length; i++){
			//this.valueTab[i] = minimumValue[i];
			listJTextField.add(new JTextField("" + valueTab[i]));
			this.valueTab[i] = valueTab[i];
		}
		display();
		button.addActionListener(this);
	}
	
	public PreviousLevel(ArrayList<JTextField> listJTextField, ArrayList<JSpinner> listJSpinner, int[] previousValue, CaracFrame frame, int[] minimumValue){
		super();
		this.frame = frame;
		for(JSpinner spinner : listJSpinner){
			this.listJTextField.add(new JTextField("" + spinner.getValue()));
		}
		for(JTextField textField : listJTextField){
			this.listJTextField.add((new JTextField(textField.getText())));
		}
		for(int i = 0; i <Fields.NUM_CARAC ;i++){
			listJLabel.add(new JLabel(Fields.getCaracName(i)));
		}
		this.listJTextField.get(Fields.NIVEAU).setText("" + (Integer.parseInt(this.listJTextField.get(Fields.NIVEAU).getText())));
		for(int i = 0; i < valueTab.length; i++){
			valueTab[i] = Integer.parseInt(this.listJTextField.get(i).getText());
		}
		for(int i = 0; i < previousValue.length; i++){
			this.previousValue[i] = previousValue[i];
		}
		for(int i = 0; i < minimumValue.length; i++){
			this.minimumValue[i] = minimumValue[i];
		}
		display();
		button.addActionListener(this);
		
		//listJTextField.get(Fields.HP-9).setText("" + (frame.calculHpMp(Fields.EPH)));
		//listJTextField.get(Fields.MP-9).setText("" + (frame.calculHpMp(Fields.EMP)));
	}
	
	public void display(){
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(5,1));
		
		JPanel jp1 = new JPanel();
		add(Fields.NIVEAU, jp1);
		add(Fields.PE, jp1);
		jp1.add(button);
		
		JPanel jp5 = new JPanel();
		add(Fields.HP, jp5);
		add(Fields.MP, jp5);
		
		JPanel jp2 = new JPanel();
		add(Fields.FORCE, jp2);
		add(Fields.AGILITE, jp2);
		add(Fields.VITALITE, jp2);
		add(Fields.DEXTERITE, jp2);
		add(Fields.INTELLIGENCE, jp2);
		add(Fields.SAGESSE, jp2);
		add(Fields.EPH,jp2);
		add(Fields.EMT,jp2);
		
		JPanel jp3 = new JPanel();
		add(Fields.TAIJUTSU, jp3);
		add(Fields.NINJUTSU, jp3);
		add(Fields.GENJUTSU, jp3);
		add(Fields.KENJUTSU,jp3);
		add(Fields.EISEI,jp3);
		add(Fields.GEKA,jp3);
		add(Fields.HIHEI,jp3);
		
		JPanel jp4 = new JPanel();
		add(Fields.EVASION, jp4);
		add(Fields.CRITIQUE, jp4);
		add(Fields.OSMOSE, jp4);
		add(Fields.MALICE, jp4);
		
		panel.add(jp5);
		panel.add(jp2);
		panel.add(jp3);
		panel.add(jp4);
		panel.add(jp1);
		
		this.add(panel);
		
		for(JTextField textField : listJTextField){
			textField.setColumns(3);
			textField.setEditable(false);
		}
	}
	
	public void add(int code, JPanel panel){
		panel.add(listJLabel.get(code));
		panel.add(listJTextField.get(code));
	}
	
	public int[] getValueTab(){
		return valueTab;
	}
	
	public String toString2(){
		String result = "";
		for(int i = 0; i < valueTab.length; i++){
			result += valueTab[i] + " ";
		}
		return result;
	}
	
	public int[] getPreviousValue(){
		return previousValue;
	}
	
	public int[] getMinimumValue(){
		return minimumValue;
	}
	
	public void actionPerformed(ActionEvent e){
		frame.levelDown(this);
	}
	
	public void print(PrintStream out) {
		out.println(toString()); 
	}
	
	@Override
	public String toString(){
		String result = "";
		result += "%ValueTab " ;
		for(int i = 0; i < valueTab.length ; i++){
			result += valueTab[i] + " ";
		}
		result += "\n" ;
		result += "%PreviousValue " ;
		for(int i = 0; i < previousValue.length ; i++){
			result += previousValue[i] + " ";
		}
		result += "\n" ;
		result += "%MinimumValue " ;
		for(int i = 0; i < minimumValue.length ; i++){
			result += minimumValue[i] + " ";
		}
		result += "\n" ;
		return result;
	}
	
	public String toGoodString(){
		int hp = Utilitaires.calculHpMp(valueTab[Fields.EPH], valueTab[Fields.NIVEAU] -1);
		int mp = Utilitaires.calculHpMp(valueTab[Fields.EMT], valueTab[Fields.NIVEAU] -1);

		String result = "";
		if(valueTab[Fields.NIVEAU] != 1){
			result += "[quote]Niveau : " + (valueTab[Fields.NIVEAU] - 1) + "\n\n";
			result += "HP : " + hp + " - MP : " + mp + "\n";
			result += "FOR : " + valueTab[Fields.FORCE] + " - AGI : " + valueTab[Fields.AGILITE] + " - VIT : " + 
					valueTab[Fields.VITALITE] + " - DEX : " + valueTab[Fields.DEXTERITE] + " - INT : " + 
					valueTab[Fields.INTELLIGENCE] + " - SAG : " + valueTab[Fields.SAGESSE] + " - EPH : " + 
					valueTab[Fields.EPH] + " - EMT : " + valueTab[Fields.EMT] + "\n";
			result += "TAI : " + valueTab[Fields.TAIJUTSU] + " - NIN : " + valueTab[Fields.NINJUTSU] + " - GEN : " + 
					valueTab[Fields.GENJUTSU] + " - KEN : " + valueTab[Fields.KENJUTSU] + " - EIS : " + 
					valueTab[Fields.EISEI] + " - HIH : " + valueTab[Fields.HIHEI] + " - GEK : " + 
					valueTab[Fields.GEKA] + "\n\n";
			result += "Evasion : " + valueTab[Fields.EVASION] + "\n";
			result += "Critique : " + valueTab[Fields.CRITIQUE] + "\n";
			result += "Osmose : " + valueTab[Fields.OSMOSE] + "\n";
			result += "Malice : " + valueTab[Fields.MALICE] + "\n\n";
			result += "Points Excédentaires : " + valueTab[Fields.PE] + "[/quote]";
		}
		return result;
	}
	
}
