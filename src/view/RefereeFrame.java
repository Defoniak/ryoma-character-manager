package view;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import mainSystem.MainSystem;

import carac.Player;
import carac.PlayerField;
import carac.Utilitaires;
import carac.Weapon;

import component.BonusMalusPanel;
import component.JCalculator;
import component.CaracPanel;
import component.JDice;
import component.PlayerPanel;
import component.TechniquePanel;
import component.WeaponPanel;



public class RefereeFrame extends JFrame implements ActionListener{
	
	JMenuBar menu = new JMenuBar();

	JMenu fileMenu = new JMenu("Fiche de gauche");
	JMenuItem fileRegister1 = new JMenuItem("Enregistrer");
	JMenuItem fileLoader1 = new JMenuItem("Charger fiche");
	JMenuItem fileBuildLoader1 = new JMenuItem("Charger signature");
	
	JMenu fileMenu2 = new JMenu("Fiche de droite");
	JMenuItem fileRegister2 = new JMenuItem("Enregistrer");
	JMenuItem fileLoader2 = new JMenuItem("Charger fiche");
	JMenuItem fileBuildLoader2 = new JMenuItem("Charger signature");
	
	JMenu dataMenu = new JMenu("base de donnee");
	JMenuItem weaponMenu = new JMenuItem("Armes");
	JMenuItem techMenu = new JMenuItem("Techniques");
	
	
	private PlayerPanel playerPanel1 = new PlayerPanel();
	private PlayerPanel playerPanel2 = new PlayerPanel();
	JDice jDice = new JDice();
	JCalculator jCalculator;
	
	JPanel flow= new JPanel();
	
	public RefereeFrame(){
		super("RyomaUtilitaire");
		Utilitaires.loadReferee(this);
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main , BoxLayout.PAGE_AXIS));
		JPanel top = new JPanel();
		top.setLayout(new BoxLayout(top , BoxLayout.LINE_AXIS));
		JPanel bot = new JPanel();
		bot.setLayout(new BoxLayout(bot, BoxLayout.LINE_AXIS));
		JPanel grid = new JPanel();
		grid.setLayout(new GridLayout(1,2));
		flow.setLayout(new FlowLayout());
		
		ArrayList<Player> playerList = new ArrayList<Player>();
		playerList.add(playerPanel1.getPlayer());
		playerList.add(playerPanel2.getPlayer());

		
		jCalculator = new JCalculator(playerList);
		
		main.add(top);
		main.add(bot);
		top.add(grid);
		bot.add(flow);
		grid.add(playerPanel1);
		grid.add(playerPanel2);
		flow.add(jDice);
		flow.add(jCalculator);
		
		
		menu.add(fileMenu);
		fileMenu.add(fileRegister1);
		fileRegister1.addActionListener(this);
		fileMenu.add(fileLoader1);
		fileLoader1.addActionListener(this);
		fileMenu.add(fileBuildLoader1);
		fileBuildLoader1.addActionListener(this);
		menu.add(fileMenu2);
		fileMenu2.add(fileRegister2);
		fileRegister2.addActionListener(this);
		fileMenu2.add(fileLoader2);
		fileLoader2.addActionListener(this);
		fileMenu2.add(fileBuildLoader2);
		fileBuildLoader2.addActionListener(this);
		
		//menu.add(dataMenu);
		dataMenu.add(weaponMenu);
		weaponMenu.addActionListener(this);
		dataMenu.add(techMenu);
		techMenu.addActionListener(this);
		this.setJMenuBar(menu);
		//main.setLayout(new GridLayout(2,1));
		//main.setLayout(blop);
		//main.add(playerPanel1);
		//main.add(getPlayerPanel2());
		add(main);
		
		setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		setSize (850, 900);
		this.setLocationRelativeTo(null);
		
		pack();
		
		
	}
	
	public PlayerPanel getPlayerPanel1() {
		return playerPanel1;
	}

	public void setPlayerPanel1(PlayerPanel playerPanel) {
		this.playerPanel1 = playerPanel1;
	}

	public PlayerPanel getPlayerPanel2() {
		return playerPanel2;
	}

	public void setPlayerPanel2(PlayerPanel playerPanel2) {
		this.playerPanel2 = playerPanel2;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//System.out.println("blop");
		JMenuItem menuItem = (JMenuItem)e.getSource();
		if(menuItem == fileRegister1){
			Utilitaires.savePlayer(playerPanel1.getPlayer());
		}
		else if(menuItem == fileRegister2){
			Utilitaires.savePlayer(playerPanel2.getPlayer());
		}
		else if(menuItem == fileLoader1){
			//SelectPlayerFrame selectPlayerFrame = new SelectPlayerFrame(this,"Selection d'une fiche joueur",true);
			String path = null;
			if(Utilitaires.getListPlayer() != null){
			path = (String)JOptionPane.showInputDialog(null, 
				      "Veuillez choisir une fiche de joueur � charger",
				      "Chargement du joueur",
				      JOptionPane.QUESTION_MESSAGE,
				      null,
				      Utilitaires.getListPlayer(),
				      null);
			}
			else{
				JOptionPane.showMessageDialog(null, "Il n'y a pas encore de build enregistr�", " erreur � R�", JOptionPane.ERROR_MESSAGE);
			}
			if(path != null){
				if(MainSystem.IN_TESTING)playerPanel1.setPlayer(Utilitaires.loadPlayer("player/" + path));
				else playerPanel1.setPlayer(Utilitaires.loadPlayer("./player/" + path));
			refresh();
			}
		}
		else if(menuItem == fileLoader2){
			//SelectPlayerFrame selectPlayerFrame = new SelectPlayerFrame(this,"Selection d'une fiche joueur",true);
			String path = null;
			if(Utilitaires.getListPlayer() != null){
			path = (String)JOptionPane.showInputDialog(null, 
				      "Veuillez choisir une fiche de joueur � charger",
				      "Chargement du joueur",
				      JOptionPane.QUESTION_MESSAGE,
				      null,
				      Utilitaires.getListPlayer(),
				      null);
			}
			else{
				JOptionPane.showMessageDialog(null, "Il n'y a pas encore de build enregistr�", " erreur � R�", JOptionPane.ERROR_MESSAGE);
			}
			if( path != null){
				if(MainSystem.IN_TESTING)playerPanel2.setPlayer(Utilitaires.loadPlayer("player/" + path));
				else playerPanel2.setPlayer(Utilitaires.loadPlayer("./player/" + path));
				refresh();
			}
		}
		else if(menuItem == fileBuildLoader1 || menuItem == fileBuildLoader2){
			String[] buildNameTab = Utilitaires.getListSignature();
			String buildName = null;
			if(buildNameTab != null){
				buildName = (String)JOptionPane.showInputDialog(null, "Quel signature voulez-vous charger ?", "Chargement de la signature", JOptionPane.QUESTION_MESSAGE,
						null, buildNameTab, buildNameTab[0]);
			}
			else{
				JOptionPane.showMessageDialog(null, "Il n'y a pas encore de signature enregistr�e", " erreur � R�", JOptionPane.ERROR_MESSAGE);
			}
			if(buildName != null){
				int[] carac = Utilitaires.calculCarac(Utilitaires.loadBuild(buildName));
				Player player;
				if(menuItem == fileBuildLoader1) player = playerPanel1.getPlayer();
				else player = playerPanel2.getPlayer();
				player.setCarac(carac);
				playerPanel1.setPlayer(player);
			}
		}
	}

	public void refresh(){
		ArrayList<Player> playerList = new ArrayList<Player>();
		playerList.add(playerPanel1.getPlayer());
		playerList.add(playerPanel2.getPlayer());
		flow.remove(jCalculator);
		jCalculator = new JCalculator(playerList);
		flow.add(jCalculator);
		this.update(getGraphics());
	}
}
