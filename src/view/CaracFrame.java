package view;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import mainSystem.MainSystem;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import carac.Fields;
import carac.Utilitaires;

public class CaracFrame extends JFrame implements ChangeListener, ActionListener, ItemListener{
	private ArrayList<JLabel> listJLabel = new ArrayList<JLabel>();
	private ArrayList<JSpinner> listJSpinner = new ArrayList<JSpinner>();
	private ArrayList<JTextField> listJTextField = new ArrayList<JTextField>();
	private ArrayList<PreviousLevel> listPreviousLevel = new ArrayList<PreviousLevel>();
	private ArrayList<String> stringBox = new ArrayList<String>();
	private JButton plus = new JButton("+");
	private JComboBox box = new JComboBox();
	private int[] previousValue = new int[9];
	private int[] minimumValue = new int[8];
	private JPanel up = new JPanel();
	private JPanel down = new JPanel();
	private JPanel main = new JPanel();
	private JPanel subDown = new JPanel();
	private CardLayout cl = new CardLayout();
	private boolean isRestauring = false;
	private boolean isLvlUp = false;
	private JMenuBar menuBar = new JMenuBar();
	private JMenu fileMenu = new JMenu("Fichier");
	private JMenu copyMenu = new JMenu("Copier");
	private JMenuItem newItem = new JMenuItem("Nouveau");
	private JMenuItem loadItem = new JMenuItem("Charger");
	private JMenuItem saveAsItem = new JMenuItem("Sauver sur");
	private JMenuItem saveItem = new JMenuItem("Sauver");
	private JMenuItem copy1Item = new JMenuItem("Copier le dernier niveau");
	private JMenuItem copy2Item = new JMenuItem("Copier tous les niveaux");
	private String buildName = null;

	public CaracFrame(){

		super ("calaculateur de caracteristiques");

		setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		setSize (850, 465);
		this.setLocationRelativeTo(null);

		fileMenu.add(newItem);
		newItem.addActionListener(new MenuItemListener());
		fileMenu.add(loadItem);
		loadItem.addActionListener(new MenuItemListener());
		fileMenu.add(saveItem);
		saveItem.addActionListener(new MenuItemListener());
		fileMenu.add(saveAsItem);
		saveAsItem.addActionListener(new MenuItemListener());
		copyMenu.add(copy1Item);
		copy1Item.addActionListener(new MenuItemListener());
		copyMenu.add(copy2Item);
		copy2Item.addActionListener(new MenuItemListener());
		menuBar.add(fileMenu);
		menuBar.add(copyMenu);
		//up.add(menuBar);
		this.setJMenuBar(menuBar);

		//main.setLayout(new GridLayout(2,1));
		main.setLayout(new BoxLayout(main, BoxLayout.PAGE_AXIS));
		JPanel superUp = new JPanel();
		superUp.setLayout(new BoxLayout(superUp, BoxLayout.LINE_AXIS));
		superUp.add(up);
		JPanel superDown = new JPanel();
		superDown.setLayout(new BoxLayout(superDown, BoxLayout.LINE_AXIS));
		superDown.add(down);

		main.add(superUp);
		main.add(superDown);
		up.setLayout(new GridLayout(5,1));
		up.setBorder(BorderFactory.createTitledBorder("Niveau actuel"));
		down.setLayout(new BorderLayout());
		down.setBorder(BorderFactory.createTitledBorder("Niveaux precedents"));


		for(int i = 0 ; i < Fields.NUM_CARAC ; i++){
			listJLabel.add(new JLabel(Fields.getCaracName(i)));
			if(i < 9){
				listJSpinner.add(new JSpinner(new SpinnerNumberModel(0,0,99,1))); 
				listJSpinner.get(i).addChangeListener(this);
				JFormattedTextField tf = ((JSpinner.DefaultEditor)listJSpinner.get(i).getEditor()).getTextField();
				tf.setEditable(false);			
			}
			else{
				listJTextField.add(new JTextField(""));
				listJTextField.get(i-9).setEditable(false);
				listJTextField.get(i-9).setColumns(3);
				listJTextField.get(i-9).setText("0");
			}
		}
		listJTextField.get(Fields.NIVEAU-9).setText("1");
		listJTextField.get(Fields.PR-9).setText("40");
		listJTextField.get(Fields.HP-9).setText("100");
		listJTextField.get(Fields.MP-9).setText("100");
		if(listJTextField.get(Fields.NIVEAU-9).getText().equals("1")){
			listJSpinner.get(Fields.PE).setEnabled(false);
		}
		plus.addActionListener(this);
		plus.setEnabled(false);
		display(up);

		JPanel flowBot = new JPanel();
		flowBot.setLayout(new FlowLayout());
		flowBot.add(box);

		subDown.setLayout(cl);
		//down.add(box, BorderLayout.NORTH);
		down.add(flowBot, BorderLayout.NORTH);
		down.add(subDown, BorderLayout.CENTER);

		add(main);
		box.addItemListener(this);
		PreviousLevel zero = new PreviousLevel(this);
		listPreviousLevel.add(zero);
		box.addItem("niveau : 1");
		subDown.add(zero, "niveau : 1");
	}

	public CaracFrame(int[] valueTab){
		this();
		for(int i = 0; i < valueTab.length ; i++){
			if (i < 9){
				listJSpinner.get(i).setValue(valueTab[i]);
			}
			else{
				listJTextField.get(i).setText("" + valueTab[i]);
			}
		}
	}

	public void actionPerformed(ActionEvent e){
		JButton button =(JButton)e.getSource();
		int niveau = Integer.parseInt(listJTextField.get(Fields.NIVEAU-9).getText());
		int pr = Integer.parseInt(listJTextField.get(Fields.PR-9).getText());
		int pe = (int)listJSpinner.get(Fields.PE).getValue();
		if(button == plus && pr == 0 && isGoodNumber()){
			for(int i = 0; i < minimumValue.length; i++){
				minimumValue[i] = (int)listJSpinner.get(i).getValue();
			}
			stringBox.add( "" + niveau);
			listJTextField.get(Fields.NIVEAU-9).setText("" +(niveau + 1));
			listJTextField.get(Fields.PR-9).setText("" + (niveau + 9 + pe));
			niveau++;
			listJTextField.get(Fields.PR-9).setText("" + (niveau + 8 + pe));
			plus.setEnabled(false);
			listJTextField.get(Fields.HP-9).setText("" + (calculHpMp(Fields.EPH)));
			listJTextField.get(Fields.MP-9).setText("" + (calculHpMp(Fields.EMT)));
			PreviousLevel previousLevel = new PreviousLevel(listJTextField, listJSpinner, previousValue, this, minimumValue);
			listPreviousLevel.add(previousLevel);
			box.addItem("niveau : " + (niveau));
			subDown.add(previousLevel, "niveau : " + (niveau));
			isLvlUp = true;
			listJSpinner.get(Fields.PE).setValue(0);
			isLvlUp = false;
			//listJTextField.get(Fields.HP-9).setText("" + (calculHpMp(Fields.EPH)));
			//listJTextField.get(Fields.MP-9).setText("" + (calculHpMp(Fields.EMP)));
		}
		if(niveau > 1){
			listJSpinner.get(Fields.PE).setEnabled(true);
		}
	}

	public void stateChanged(ChangeEvent e){
		JSpinner spinner = (JSpinner)e.getSource(); // spinner appuye
		int niveau = Integer.parseInt(listJTextField.get(Fields.NIVEAU-9).getText());
		int code = searchSpinner(spinner); // numero du champ
		int pr = Integer.parseInt((listJTextField.get(Fields.PR-9).getText())); // nombre de points restants
		if((pr == 0 && isUp(spinner, code) )|| blop(code) && isUp(spinner, code)){
			listJSpinner.get(code).setValue(previousValue[code]);
		}
		caracSecondaire(code);
		caracTertiaire(code);
		if((int)spinner.getValue() > previousValue[code]){ // up press
			if(niveau == 1 && (int)spinner.getValue() > 9 && ! isRestauring){
				spinner.setValue(9);
			}
			else{
				listJTextField.get(Fields.PR-9).setText("" + (pr - 1*pointUsed(niveau,(int)spinner.getValue(), spinner)));
			}
		}
		else if((int)spinner.getValue() < previousValue[code]){
			decrease(code, spinner);
		}
		previousValue[code] = (int)spinner.getValue();

		if(isGoodNumber() && Integer.parseInt((listJTextField.get(Fields.PR-9).getText())) == 0){ // gestion de la disponibilit� du bouton plus
			plus.setEnabled(true);
		}
		else if(Integer.parseInt((listJTextField.get(Fields.PR-9).getText())) != 0){ 
			plus.setEnabled(false);
		}
		else{
		}
	}

	public void itemStateChanged(ItemEvent e){
		if(box.getSelectedItem() != null){
			cl.show(subDown, box.getSelectedItem().toString());
		}
	}

	public class MenuItemListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			JMenuItem item = (JMenuItem)e.getSource();
			if(item == saveItem){
				//callSaveFrame();
				save2();
			}
			else if(item == saveAsItem){
				//callSaveAsFrame();
				saveAs2();
			}
			else if(item == loadItem){
				//callLoadFrame();
				load2();
			}
			else if(item == newItem){
				callNewFrame();
			}
			else if(item == copy1Item){
				Clipboard blop = new Clipboard(" ");
				//ClipboardOwner blopeuh = 
				//System.out.println("Clipboard contains:" + blopeuh.getClipboardContents() );
				StringSelection stringSelection = new StringSelection(toGoodString());
				Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				clipboard.setContents( stringSelection, null );
			}
			else if(item == copy2Item){
				Clipboard blop = new Clipboard(" ");
				//ClipboardOwner blopeuh = 
				//System.out.println("Clipboard contains:" + blopeuh.getClipboardContents() );
				StringSelection stringSelection = new StringSelection(toGigaString());
				Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				clipboard.setContents( stringSelection, null );
			}
		}
	}


	public int caracSecondaire(int carac1, int carac2){
		return (int)Math.ceil((double)((int)listJSpinner.get(carac1).getValue() + (int)listJSpinner.get(carac2).getValue())/2);
	}

	public void display(JPanel main){
		JPanel jp1 = new JPanel();
		add(Fields.PR,jp1);
		add(Fields.HP,jp1);
		add(Fields.MP,jp1);
		add(Fields.PE,jp1);
		main.add(jp1);

		JPanel jp2 = new JPanel();
		add(Fields.FORCE,jp2);
		add(Fields.AGILITE,jp2);
		add(Fields.VITALITE,jp2);
		add(Fields.DEXTERITE,jp2);
		add(Fields.INTELLIGENCE,jp2);
		add(Fields.SAGESSE,jp2);
		add(Fields.EPH,jp2);
		add(Fields.EMT,jp2);
		main.add(jp2);

		JPanel jp3 = new JPanel();
		add(Fields.TAIJUTSU,jp3);
		add(Fields.NINJUTSU,jp3);
		add(Fields.GENJUTSU,jp3);
		add(Fields.KENJUTSU,jp3);
		add(Fields.EISEI,jp3);
		add(Fields.GEKA,jp3);
		add(Fields.HIHEI,jp3);
		main.add(jp3);

		JPanel jp4 = new JPanel();
		add(Fields.EVASION,jp4);
		add(Fields.CRITIQUE,jp4);
		add(Fields.OSMOSE,jp4);
		add(Fields.MALICE,jp4);
		main.add(jp4);

		JPanel jp5 = new JPanel();
		add(Fields.NIVEAU,jp5);
		jp5.add(plus);
		main.add(jp5);
	}

	public void add(int caracCode, JPanel jp){
		jp.add(listJLabel.get(caracCode));
		if(caracCode < 9){
			jp.add(listJSpinner.get(caracCode));
		}
		else{
			jp.add(listJTextField.get(caracCode-9));
		}
	}

	public int searchSpinner(JSpinner spinner){
		for(int i = 0;i < listJSpinner.size() ; i++){
			if(listJSpinner.get(i) == spinner){
				return i ;
			}
		}
		return 0;
	}

	public void caracSecondaire(int caracCode){
		switch(caracCode){
		case Fields.FORCE: 
			listJTextField.get(Fields.TAIJUTSU-9).setText("" + caracSecondaire(Fields.FORCE,Fields.AGILITE));
			listJTextField.get(Fields.HIHEI-9).setText("" + caracSecondaire(Fields.FORCE,Fields.INTELLIGENCE));
		case Fields.AGILITE:
			listJTextField.get(Fields.TAIJUTSU-9).setText("" + caracSecondaire(Fields.FORCE,Fields.AGILITE));
			listJTextField.get(Fields.KENJUTSU-9).setText("" + caracSecondaire(Fields.AGILITE,Fields.DEXTERITE));
		case Fields.VITALITE:
			listJTextField.get(Fields.EISEI-9).setText("" + caracSecondaire(Fields.INTELLIGENCE,Fields.VITALITE));
			//if(listJTextField.get(Fields.NIVEAU-9).getText().equals("1"))listJTextField.get(Fields.HP-9).setText("" + calculHpMp(Fields.VITALITE));
		case Fields.DEXTERITE:
			listJTextField.get(Fields.NINJUTSU-9).setText("" + caracSecondaire(Fields.DEXTERITE,Fields.SAGESSE));
			listJTextField.get(Fields.GEKA-9).setText("" + caracSecondaire(Fields.DEXTERITE,Fields.INTELLIGENCE));
			listJTextField.get(Fields.KENJUTSU-9).setText("" + caracSecondaire(Fields.AGILITE,Fields.DEXTERITE));
		case Fields.INTELLIGENCE:
			listJTextField.get(Fields.GENJUTSU-9).setText("" + caracSecondaire(Fields.SAGESSE,Fields.INTELLIGENCE));
			listJTextField.get(Fields.EISEI-9).setText("" + caracSecondaire(Fields.VITALITE,Fields.INTELLIGENCE));
			listJTextField.get(Fields.GEKA-9).setText("" + caracSecondaire(Fields.DEXTERITE,Fields.INTELLIGENCE));
			listJTextField.get(Fields.HIHEI-9).setText("" + caracSecondaire(Fields.FORCE,Fields.INTELLIGENCE));
			//if(listJTextField.get(Fields.NIVEAU-9).getText().equals("1"))listJTextField.get(Fields.MP-9).setText("" + calculHpMp(Fields.INTELLIGENCE));
		case Fields.SAGESSE:
			listJTextField.get(Fields.GENJUTSU-9).setText("" + caracSecondaire(Fields.SAGESSE,Fields.INTELLIGENCE));
			listJTextField.get(Fields.NINJUTSU-9).setText("" + caracSecondaire(Fields.SAGESSE,Fields.DEXTERITE));

		case Fields.EPH:
			listJTextField.get(Fields.HP-9).setText("" + calculHpMp(Fields.EPH));
		case Fields.EMT:
			listJTextField.get(Fields.MP-9).setText("" + calculHpMp(Fields.EMT));
		}
	}

	public void caracTertiaire(int caracCode){
		switch(caracCode){
		case Fields.AGILITE: 
			listJTextField.get(Fields.EVASION-9).setText("" + ( 5 + (((int)listJSpinner.get(Fields.AGILITE).getValue() - 1)/10)*5));
		case Fields.DEXTERITE:
			listJTextField.get(Fields.CRITIQUE-9).setText("" + ( 5 + (((int)listJSpinner.get(Fields.DEXTERITE).getValue() - 1)/10)*5));
		case Fields.INTELLIGENCE:
			listJTextField.get(Fields.MALICE-9).setText("" + (((int)listJSpinner.get(Fields.INTELLIGENCE).getValue() - 1)/10)*5);
		case Fields.SAGESSE:
			listJTextField.get(Fields.OSMOSE-9).setText("" + ( 5 + (((int)listJSpinner.get(Fields.SAGESSE).getValue() - 1)/10)*5));
		}
	}

	public boolean isUp(JSpinner spinner, int code){
		if((int)spinner.getValue() > previousValue[code]){ // up press
			return true;
		}
		else if((int)spinner.getValue() <= previousValue[code]){
			return false;
		}
		return false;
	}

	public boolean isGoodNumber(){
		if(! listJTextField.get(Fields.NIVEAU-9).getText().equals("1")){
			return true;
		}
		for(JSpinner spinner : listJSpinner){
			if((int)spinner.getValue() == 0 || (int)spinner.getValue() > 9){
				if(searchSpinner(spinner) != Fields.PE){
					return false;
				}
			}
		}
		return true;
	}

	public int pointUsed(int niveau, int pt, JSpinner spinner){
		if(niveau == 1 || searchSpinner(spinner) == Fields.PE){
			return 1;
		}
		int result = 1 + (int)(Math.ceil(((double)pt-1)/10.0));
		return result;
	}

	public boolean blop(int code){
		//si nombre de pt restant plus petit que pointUsed return false
		if(Integer.parseInt(listJTextField.get(Fields.PR-9).getText()) >= pointUsed(Integer.parseInt(listJTextField.get(Fields.NIVEAU-9).getText()),(int)listJSpinner.get(code).getValue(),listJSpinner.get(code))){
			return false;
		}
		return true;
	}

	public int calculHpMp(int code){
		int result = 0;
		result = 100 + 20*(int)listJSpinner.get(code).getValue();
		/*for(PreviousLevel lvl: listPreviousLevel){
			if(lvl.getValueTab()[Fields.NIVEAU] > 1){
				result += 2*(lvl.getValueTab()[Fields.NIVEAU]+8);
			}
		}*/
		for(int i = 2; i <= Integer.parseInt(listJTextField.get(Fields.NIVEAU-9).getText()); i++){
			result += (i + 8)*2;
		}
		return result;
	}

	public void levelDown(PreviousLevel lvl){
		isRestauring = true;
		int[] valueTab = lvl.getValueTab();
		int[] oldValue = lvl.getPreviousValue();
		while(listPreviousLevel.size() != (valueTab[Fields.NIVEAU])){
			box.removeItemAt(box.getItemCount()-1);
			listPreviousLevel.remove(listPreviousLevel.get(listPreviousLevel.size()-1));
			//System.out.println(box.getItemCount());
		}
		for(int i = 0; i < valueTab.length ; i++){
			if (i < 9){
				listJSpinner.get(i).setValue(valueTab[i]);
			}
			else{
				listJTextField.get(i-9).setText("" + valueTab[i]);
			}
		}
		for(int i = 0; i < oldValue.length; i++){
			previousValue[i] = oldValue[i];
		}
		for(int i = 0; i < minimumValue.length; i++){
			minimumValue[i] = lvl.getMinimumValue()[i];
		}
		listJTextField.get(Fields.NIVEAU-9).setText("" + (valueTab[Fields.NIVEAU]));
		listJSpinner.get(Fields.PE).setValue(0);
		listJTextField.get(Fields.HP-9).setText("" + calculHpMp(Fields.EPH));
		listJTextField.get(Fields.MP-9).setText("" + calculHpMp(Fields.EMT));
		isRestauring = false;
		if(listJTextField.get(Fields.NIVEAU-9).getText().equals("1")){
			listJSpinner.get(Fields.PE).setEnabled(false);
		}
	}

	public void decrease(int code, JSpinner spinner){
		if(code != Fields.PE){
			if ((int)spinner.getValue() < minimumValue[code] && !isRestauring){
				spinner.setValue(minimumValue[code]);
			}
			else{
				listJTextField.get(Fields.PR-9).setText("" + (Integer.parseInt(listJTextField.get(Fields.PR-9).getText()) + 1*pointUsed(Integer.parseInt(listJTextField.get(Fields.NIVEAU-9).getText()),(int)spinner.getValue()+1, spinner)));
			}
		}
		else if(! isLvlUp && !isRestauring){
			listJTextField.get(Fields.PR-9).setText("" + (Integer.parseInt(listJTextField.get(Fields.PR-9).getText()) + 1*pointUsed(Integer.parseInt(listJTextField.get(Fields.NIVEAU-9).getText()),(int)spinner.getValue()+1, spinner)));

		}
	}

	@Override
	public String toString(){
		String result = "";
		for(PreviousLevel lvl : listPreviousLevel){
			result += lvl.toString();
			result += "\n";
		}
		return result;
	}

	public void callSaveFrame(){
		System.out.println(this.buildName);
		if(buildName != null){
			//Profil profil = new Profil(this, buildName);
			Utilitaires.save(this);
		}
		else{
			callSaveAsFrame();
		}
	}

	public void callSaveAsFrame(){
		/*JOptionPane jop = new JOptionPane();
		String buildName = "";
		int i = 0;
		while(! Utilitaires.isGoodBuildName(buildName)){
			if(i >= 1){
				JOptionPane jpo = new JOptionPane();
				jpo.showMessageDialog(null, "Veuillez introduire un nom de fichier correct !", "Erreur", JOptionPane.ERROR_MESSAGE);
			}
			buildName = jop.showInputDialog(null, "veuillez choisir un nom pour ce build", "Sauvegarde du build", JOptionPane.QUESTION_MESSAGE);
			i++;
		}

		if(buildName == null){

		}
		else if( buildName != null || !buildName.equals("")){
			Profil profil = new Profil(this, buildName);
		}*/

		//String buildName = Utilitaires.save(this);
		if(buildName != null){
			this.buildName = buildName;
		}
	}

	public void callLoadFrame(){
		/*String[] buildNameTab = Utilitaires.buildTab();
		JOptionPane jop = new JOptionPane();
		String buildName = (String)jop.showInputDialog(null, "Quel build voulez-vous charger ?", "Chargement du build", JOptionPane.QUESTION_MESSAGE,
				null, buildNameTab, buildNameTab[0]);
		if(buildName != null){
			ArrayList<PreviousLevel> listLvl = Utilitaires.load(buildName, this);
			loadProfil(listLvl);
		}
		this.buildName = buildName;*/
		String buildName = Utilitaires.openFile();
		if(buildName != null){
			this.buildName = buildName;
			ArrayList<PreviousLevel> listLvl = Utilitaires.load(this);
			loadProfil(listLvl);
		}


	}

	public void callNewFrame(){
		MainSystem.newFrame(this);
	}

	public void loadProfil(ArrayList<PreviousLevel> listLvl){
		this.listPreviousLevel = listLvl;
		levelDown(listLvl.get(listLvl.size()-1));
		box.removeAllItems();
		for(PreviousLevel lvl : listLvl){
			box.addItem("niveau : " + lvl.getValueTab()[Fields.NIVEAU]);
			subDown.add(lvl, "niveau : " + (lvl.getValueTab()[Fields.NIVEAU]));
		}
		if(!listJTextField.get(Fields.NIVEAU-9).getText().equals("1")){
			listJSpinner.get(Fields.PE).setEnabled(true);
		}
		else{
			listJSpinner.get(Fields.PE).setEnabled(false);
		}
	}

	public String getBuildName() {
		return buildName;
	}

	public void setBuildName(String buildName) {
		this.buildName = buildName;
	}

	public void save2(){
		System.out.println(this.buildName);
		if(buildName != null){
			//Profil profil = new Profil(this, buildName);
			Utilitaires.save(this);
		}
		else{
			saveAs2();
		}
	}

	public void saveAs2(){
		String buildName = "";
		int i = 0;
		while(buildName.equals("")){
			if(i >= 1){
				JOptionPane.showMessageDialog(null, "Veuillez introduire un nom de fichier correct !", "Erreur", JOptionPane.ERROR_MESSAGE);
			}
			buildName = JOptionPane.showInputDialog(null, "veuillez choisir un nom pour cette signature", "Sauvegarde de la signature", JOptionPane.QUESTION_MESSAGE);
			i++;
		}
		if( buildName != null || !buildName.equals("")){
			this.buildName = buildName + ".txt";
			Utilitaires.save(this);
		}
	}

	public void load2(){
		String[] buildNameTab = Utilitaires.getListSignature();
		String buildName = null;
		if(buildNameTab != null){
			buildName = (String)JOptionPane.showInputDialog(null, "Quel signature voulez-vous charger ?", "Chargement de la signature", JOptionPane.QUESTION_MESSAGE,
					null, buildNameTab, buildNameTab[0]);
		}
		else{
			JOptionPane.showMessageDialog(null, "Il n'y a pas encore de signature enregistr�e", " erreur � R�", JOptionPane.ERROR_MESSAGE);
		}
		if(buildName != null){
			this.buildName = buildName;
			ArrayList<PreviousLevel> listLvl = Utilitaires.load(this);
			loadProfil(listLvl);
		}
	}

	public String toGoodString(){
		String result = "";
		result += "Niveau : " + listJTextField.get(Fields.NIVEAU-9).getText() + "\n\n";
		result += "HP : " + listJTextField.get(Fields.HP-9).getText() + " - MP : " + listJTextField.get(Fields.MP-9).getText() + "\n";
		result += "FOR : " + listJSpinner.get(Fields.FORCE).getValue() + " - AGI : " + listJSpinner.get(Fields.AGILITE).getValue() + " - VIT : " + 
				listJSpinner.get(Fields.VITALITE).getValue() + " - DEX : " + listJSpinner.get(Fields.DEXTERITE).getValue() + " - INT : " + 
				listJSpinner.get(Fields.INTELLIGENCE).getValue() + " - SAG : " + listJSpinner.get(Fields.SAGESSE).getValue() + " - EPH : " + 
				listJSpinner.get(Fields.EPH).getValue() + " - EMT : " + listJSpinner.get(Fields.EMT).getValue() + "\n";
		result += "TAI : " + listJTextField.get(Fields.TAIJUTSU-9).getText() + " - NIN : " + listJTextField.get(Fields.NINJUTSU-9).getText() + " - GEN : " + 
				listJTextField.get(Fields.GENJUTSU-9).getText() + " - KEN : " + listJTextField.get(Fields.KENJUTSU-9).getText() + " - EIS : " + 
				listJTextField.get(Fields.EISEI-9).getText() + " - HIH : " + listJTextField.get(Fields.HIHEI-9).getText() + " - GEK : " + 
				listJTextField.get(Fields.GEKA-9).getText() + "\n\n";
		result += "Evasion : " + listJTextField.get(Fields.EVASION-9).getText() + "\n";
		result += "Critique : " + listJTextField.get(Fields.CRITIQUE-9).getText() + "\n";
		result += "Osmose : " + listJTextField.get(Fields.OSMOSE-9).getText() + "\n";
		result += "Malice : " + listJTextField.get(Fields.MALICE-9).getText() + "\n\n";
		result += "Points Exc�dentaires : " + listJSpinner.get(Fields.PE).getValue();
		return result;
	}
	
	public String toGigaString(){
		String result = "";
		for(PreviousLevel lvl : listPreviousLevel){
			result += lvl.toGoodString() + "\n";
		}
		result += "[quote]" + this.toGoodString() + "[/quote]";
		return result;
	}
}
