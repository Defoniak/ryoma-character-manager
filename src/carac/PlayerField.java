package carac;

public class PlayerField {
	public static final int FQ = 0, MQ = 1, HQ = 2, CQ = 3, LQ = 4, SQ = 5;
	public static final int LEGER = 0, RENFORCE = 1, TENUE = 2, COMBINAISON = 3, CUIRASSE = 4, ARMURE = 5;

	public static final int FORCE = 0, AGILITE = 1, VITALITE = 2, DEXTERITE = 3, INTELLIGENCE = 4, SAGESSE = 5, EPH = 6, EMP = 7;
	public static final int TAIJUTSU = 8, NINJUTSU = 9, GENJUTSU = 10, KENJUTSU = 11, EISEI = 12, GEKA = 13, HIHEI = 14;
	public static final int EVASION = 15, CRITIQUE = 16, MALICE = 17, OSMOSE = 18;
	public static final int HP = 19, MP = 20, NIVEAU = 21;
	
	public static final int CARAC_LENGTH = 22;
	public static final int RANK_LENGHT = 6;
	public static final int QUALITY_LENGTH = 6;
	
	public static final int D = 0, C = 1, B = 2, A = 3, S = 4, INTERDITE = 5;
	public static final int ARCANE = 0, PHYSIQUE = 1, SPECIALE = 2, ILLUSION = 3, INVOCATION = 4, COMPLEMENTAIRE = 5;
	public static final int BOTH = 0, CAC = 1, DISTANCE = 2;

	public static final int ASPIRANT = 0, GENIN = 1, CHUUNIN = 2, JUUNIN = 3, ANBU = 4, SANIN_KAGE = 5;
	
	
	public static String getQualityName(int qualityCode){
		String[] qualityName = {"FQ","MQ","HQ","CQ","LQ","SQ"};
		return qualityName[qualityCode];
	}
	
	public static String getTypeName(int typeCode){
		String[] typeName = {"Arcane","Physique","Speciale","Illusion","Invocation","Complementaire"};
		return typeName[typeCode];
	}
	
	public static String getRankName(int rankCode){
		String[] rankName = {"Aspirant","Genin","Chuunin","Juunin","Anbu","Sannin - Kage"};
		return rankName[rankCode];
	}

	public static String getArmorName(int armorCode){
		String[] armorName = {"Vetement leger","Vetement renforce","Tenue de combat","Combinaison ninja","Cuirasse ninja","Armure ninja"};
		return armorName[armorCode];
	}

	public static int getQuality(int qualityCode){
		int[] quality = {0,3,5,8,12,16};
		return quality[qualityCode];
	}

	public static int getArmor(int armorCode){
		int[] armor = {3,6,9,12,15,18};
		return armor[armorCode];
	}

	public static String getGradeName(int gradeCode){
		String result = "Technique ";
		if(gradeCode != INTERDITE){
			result += "de grade ";
		}
		String[] gradeName = {"D","C","B","A","S","interdite"};
		return result + gradeName[gradeCode];
	}
	
	public static String getCaracName(int caracCode){
		String[] caracName = {"force : ", "agilite : ", "vitalite : ", "dexterite : ", "intelligence : ", "sagesse : ", "EPH : ", "EMP : ",
				"taijutsu : ", "ninjutsu : ", "genjutsu : ", "kenjutsu : ", "eisei : ", "geka : ", "hihei : ",
				"evasion : ", "critique : ", "malice : ", "osmose : ", 
				"hp : ", "mp : ", "niveau : "};

		return caracName[caracCode];
	}
	
	public static String getRangeName(int rangeCode){
		String[] rangeName = {"Utilisation a distance et au corps a corps","Utilisation au corps a corps","Utilisation a distance"};
		return rangeName[rangeCode];
	}
}
