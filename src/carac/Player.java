package carac;

import java.io.PrintStream;
import java.util.ArrayList;

public class Player {
	
	private int[] carac = new int[PlayerField.CARAC_LENGTH];
	
	private int grade = 0;
	private int armor = 0;
	private int armorQuality = 0;
	private ArrayList<Weapon> weaponList = new ArrayList<Weapon>();
	private ArrayList<Technique> techList = new ArrayList<Technique>();
	private String bonus = "";
	//private boolean kido = false;
	
	private String name = "Unknown";
	
	
	public Player(){
		for(int i = 0; i < carac.length ; i++){
			carac[i] = 0;
		}
	}
	
	public int[] getCarac(){
		return carac;
	}
	
	public void setCarac(int[] carac){
		this.carac = carac;
	}
	
	public int getCarac(int code){
		return carac[code];
	}
	
	public void setCarac(int code, int value){
		carac[code] = value;
	}

	public int getArmor() {
		return armor;
	}

	public void setArmor(int armor) {
		this.armor = armor;
	}

	public int getArmorQuality() {
		return armorQuality;
	}

	public void setAmorQuality(int amorQuality) {
		this.armorQuality = amorQuality;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTotalArmor() {
		return armorQuality + armor;
	}

	public ArrayList<Weapon> getWeaponList() {
		return weaponList;
	}

	public void setWeaponList(ArrayList<Weapon> weaponList) {
		this.weaponList = weaponList;
	}
	
	public String toString(){
		return name;
	}

	public ArrayList<Technique> getTechList() {
		return techList;
	}

	public void setTechList(ArrayList<Technique> techList) {
		this.techList = techList;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getBonus() {
		return bonus;
	}

	public void setBonus(String bonus) {
		this.bonus = bonus;
	}
	
	/*public boolean isKido() {
		return kido;
	}*/

	/*public void setKido(boolean kido) {
		/*if(this.kido == false && kido == true){
			carac[PlayerField.MP] /= 2 ;
		}
		else if(this.kido == true && kido == false){
			carac[PlayerField.MP] *= 2 ;
		}
		this.kido = kido;
	} */

	public String toSuperString(){
		String result = "";
		result += name + "\n";
		result += grade + " " + armor + " " + armorQuality/* + " " + ((kido == true)? "0" : "1")*/ + "\n";
		result += bonus + "\n";
		for(int i = 0; i < carac.length; i++){
			result += carac[i] + " ";
		}
		result += "\n" + "$WEAPON" + "\n\n" ;
		for(Weapon weapon : weaponList){
			result += weapon.toSuperString();
		}
		result += "$TECHNIQUE";
		for(Technique tech : techList){
			result += "\n\n" + tech.getName() + "\n" + tech.getText();
		}
		return result;
	}
}
