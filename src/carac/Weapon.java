package carac;

public class Weapon {
	
	private String name = "";
	private int atk = 0;
	private int quality = 0;
	private int realAtk = 0;
	
	
	public Weapon(){
	}
	
	public Weapon(String name, int atk){
		this.name = name;
		this.atk = atk;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getAtk() {
		return atk;
	}


	public void setAtk(int atk) {
		this.atk = atk;
	}
	
	
	
	public String toString(){
		if(quality != -1) return name + " " + PlayerField.getQualityName(getQuality());
		else return name;
	}
	
	public String toSuperString(){
		return name + "\n" + atk + "\n" +quality + "\n \n";
	}

	public int getRealAtk() {
		if(quality != -1)return atk + PlayerField.getQuality(getQuality());
		else return atk;
	}

	public void setRealAtk(int realAtk) {
		this.realAtk = realAtk;
	}

	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}
}
