package carac;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;

import mainSystem.MainSystem;

import view.CaracFrame;
import view.PreviousLevel;
import view.RefereeFrame;

public class Utilitaires {

	public static void save(CaracFrame frame){
		//String buildName = selectFile();
		String buildName = frame.getBuildName();
		if(buildName != null){
			try{
				File f = new File("");
				if(MainSystem.IN_TESTING)f = new File("signature/" + buildName);
				else f = new File("./signature/" + buildName);
				PrintWriter pw = new PrintWriter (new BufferedWriter (new FileWriter (f)));

				pw.print(frame);
				pw.close();
			}
			catch(IOException exception){
				System.out.println("Erreur lors de la lecture : " + exception.getMessage());
			}
		}
		//return buildName;
	}

	public static void savePlayer(Player player){
		try{
			File f = new File("");
			if(MainSystem.IN_TESTING)f = new File("player/" + player + ".txt");
			else f = new File("./player/" + player +".txt");
			//File f = new File("../RyomaUtilitaire2_0/player/" + player +".txt");
			//File f = new File("player/" + player + ".txt");
			PrintWriter pw = new PrintWriter (new BufferedWriter (new FileWriter (f)));

			pw.print(player.toSuperString());
			pw.close();
		}
		catch(IOException exception){
			System.out.println("Erreur lors de la lecture : " + exception.getMessage());
		}
	}
	
	public static String[] getListPlayer(){
		File directory = new File("");
		if(MainSystem.IN_TESTING)directory = new File("player");
		else directory = new File("./player");
		//File directory = new File("player");
		//File directory = new File("../RyomaUtilitaire2_0/player");
		if(!directory.exists()){
			System.out.println("Le fichier/r�pertoire n'existe pas");
			return null;
		}else if(!directory.isDirectory()){
			System.out.println("Le chemin correspond � un fichier et non � un r�pertoire");
			return null;
		}else{
			File[] subFiles = directory.listFiles();
			String message = "Le r�pertoire contient "+ subFiles.length+" fichier"+(subFiles.length>1?"s":"");
			System.out.println(message);
			String[] listPlayer = new String[subFiles.length];
			for(int i=0 ; i<subFiles.length; i++){
				listPlayer[i] = subFiles[i].getName();
			}
			return listPlayer;
		}
	}
	
	public static String[] getListSignature(){
		File directory = new File("");
		if(MainSystem.IN_TESTING)directory = new File("signature");
		else directory = new File("./signature");
		//File directory = new File("signature");
		//File directory = new File("../RyomaUtilitaire2_0/signature");
		if(!directory.exists()){
			System.out.println("Le fichier/r�pertoire n'existe pas");
			return null;
		}else if(!directory.isDirectory()){
			System.out.println("Le chemin correspond � un fichier et non � un r�pertoire");
			return null;
		}else{
			File[] subFiles = directory.listFiles();
			String message = "Le r�pertoire contient "+ subFiles.length+" fichier"+(subFiles.length>1?"s":"");
			System.out.println(message);
			String[] listSignature = new String[subFiles.length];
			for(int i=0 ; i<subFiles.length; i++){
				listSignature[i] = subFiles[i].getName();
			}
			return listSignature;
		}
	}

	public static Player loadPlayer(String path){
		Player player = new Player();
	//	System.out.println(path);
		try
		{
			File f = new File(path);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader (fr);
			try
			{
				String line = br.readLine();
				player.setName(line);
				line = br.readLine();
				Pattern pattern = Pattern.compile("(\\d{1}) (\\d{1}) (\\d{1}) (\\d{1})");
				Matcher matcher = pattern.matcher(line);
				if(matcher.matches()){
					player.setGrade(Integer.parseInt(matcher.group(1)));
					player.setArmor(Integer.parseInt(matcher.group(2)));
					player.setAmorQuality(Integer.parseInt(matcher.group(3)));
					//player.setKido((Integer.parseInt(matcher.group(4)) == 0)? true : false);

				}
				line = br.readLine();
				player.setBonus(line);
				line = br.readLine();
				pattern = Pattern.compile("\\d+");
				matcher = pattern.matcher(line);
				for(int i = 0; matcher.find(); i++){
					//System.out.println(matcher.group());
					player.setCarac(i, Integer.parseInt(matcher.group()));
				}
				line = br.readLine();
				line = br.readLine();
				line = br.readLine();
				ArrayList<Weapon> listWeapon = new ArrayList<Weapon>();
				while(! line.equals("$TECHNIQUE")){
					Weapon weapon = new Weapon();
					weapon.setName(line);
					line = br.readLine();
					weapon.setAtk(Integer.parseInt(line));
					line = br.readLine();
					weapon.setQuality(Integer.parseInt(line));
					line = br.readLine();
					line = br.readLine();
					listWeapon.add(weapon);
				}
				player.setWeaponList(listWeapon);
				ArrayList<Technique> listTech = new ArrayList<Technique>();
				line = br.readLine();
				while(line != null){
					Technique tech = new Technique();
					line = br.readLine();
					tech.setName(line);
					line = br.readLine();
					tech.setText(line);
					listTech.add(tech);
					line = br.readLine();
				}
				player.setTechList(listTech);
				br.close();
				fr.close();
			}
			catch (IOException exception)
			{
				System.out.println ("Erreur lors de la lecture : " + exception.getMessage());
			}
		}
		catch (FileNotFoundException exception)
		{
			System.out.println ("Le fichier n'a pas �t� trouv�");
		}
		return player;
	}

	public static void loadReferee(RefereeFrame referee){
		loadWeapon(referee);
		loadTech(referee);
	}

	public static void loadTech(RefereeFrame referee){
		ArrayList<Technique> techList = new ArrayList<Technique>();
		try
		{
			File f = new File("");
			if(MainSystem.IN_TESTING)f = new File("data/tech.txt");
			else f = new File("./data/tech.txt");
			//File f = new File("../RyomaUtilitaire2_0/data/tech.txt");
			//File f = new File("data/tech.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader (fr);
			try
			{
				String line = br.readLine();
				while(line != null){
					//Pattern pattern = Pattern.compile("(\\w+) (\\d+)");
					//Matcher matcher = pattern.matcher(line);
					Technique tech = new Technique();
					while(! line.isEmpty()){
						tech.setName(line);
						line = br.readLine();
						tech.setText(line);
						line = br.readLine();
					}
					techList.add(tech);
					line = br.readLine();
				}
				br.close();
				fr.close();
			}
			catch (IOException exception)
			{
				System.out.println ("Erreur lors de la lecture : " + exception.getMessage());
			}
		}
		catch (FileNotFoundException exception)
		{
			System.out.println ("Le fichier n'a pas �t� trouv�");
		}
		referee.getPlayerPanel1().setDataTech(techList);
		referee.getPlayerPanel2().setDataTech(techList);
	}

	public static void loadWeapon(RefereeFrame referee){
		ArrayList<Weapon> weaponList = new ArrayList<Weapon>();
		try
		{
			File f = new File("");
			if(MainSystem.IN_TESTING)f = new File("data/weapon.txt");
			else f = new File("./data/weapon.txt");
			//File f = new File("../RyomaUtilitaire2_0/data/weapon.txt");
			//File f = new File("data/weapon.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader (fr);
			try
			{
				String line = br.readLine();
				while(line != null){
					Pattern pattern = Pattern.compile("(\\w+) (\\d+)");
					Matcher matcher = pattern.matcher(line);
					if(matcher.matches()){
						weaponList.add(new Weapon(matcher.group(1),Integer.parseInt(matcher.group(2))));
					}
					line = br.readLine();
				}
				br.close();
				fr.close();
			}
			catch (IOException exception)
			{
				System.out.println ("Erreur lors de la lecture : " + exception.getMessage());
			}
		}
		catch (FileNotFoundException exception)
		{
			System.out.println ("Le fichier n'a pas �t� trouv�");
		}

		referee.getPlayerPanel1().setDataWeapon(weaponList);
		referee.getPlayerPanel2().setDataWeapon(weaponList);
	}

	public static String selectFile(){
		File f = null;
		JFileChooser fc = new JFileChooser();
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
		int returnVal = fc.showSaveDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			f = fc.getSelectedFile();
		}
		if(f != null){
			return f.getAbsolutePath();
		}
		else{
			return null;
		}
		//return f.getAbsolutePath();
	}

	public static String openFile(){
		File f = null;
		JFileChooser fc = new JFileChooser();
		fc.setDialogType(JFileChooser.OPEN_DIALOG);
		fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
		int returnVal = fc.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			f = fc.getSelectedFile();
		}
		if(f != null){
			return f.getAbsolutePath();
		}
		else{
			return null;
		}
		//return f.getAbsolutePath();
	}

	public static ArrayList<PreviousLevel> load(CaracFrame frame){
		ArrayList<PreviousLevel> listLvl = new ArrayList<PreviousLevel>();
		String buildName = frame.getBuildName();
		try
		{
			File f = new File("");
			if(MainSystem.IN_TESTING)f = new File("signature/" + buildName);
			else f = new File("./signature/" + buildName);
			//File f = new File("signature/" + buildName);
			//File f = new File("../RyomaUtilitaire2_0/signature/" + buildName);
			
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader (fr);
			try
			{
				String line = br.readLine();
				while(line != null){
					int[] valueTab = new int[Fields.NUM_CARAC];
					int[] previousValue = new int[9];
					int[] minimumValue = new int[8];
					while(!line.isEmpty()){
						Pattern pattern = Pattern.compile("\\d+");
						Matcher matcher = pattern.matcher(line);
						if(line.substring(0, 2).equals("%V")){
							/*for(int i = 0; matcher.find(); i++){
								valueTab[i] = Integer.parseInt(matcher.group());
							}*/
							int i = 0;
							while(matcher.find()){
								valueTab[i] = Integer.parseInt(matcher.group());
								i++;
							}
						}
						else if(line.substring(0,2).equals("%P")){
							/*for(int i = 0; matcher.find(); i++){
								previousValue[i] = Integer.parseInt(matcher.group());
							}*/
							int i = 0;
							while(matcher.find()){
								previousValue[i] = Integer.parseInt(matcher.group());
								i++;
							}
						}
						else if(line.substring(0,2).equals("%M")){
							/*for(int i = 0; matcher.find(); i++){
								minimumValue[i] = Integer.parseInt(matcher.group());
							}*/
							int i = 0;
							while(matcher.find()){
								minimumValue[i] = Integer.parseInt(matcher.group());
								i++;
							}
						}
						line = br.readLine();
					}
					PreviousLevel lvl = new PreviousLevel(valueTab, previousValue, minimumValue, frame);
					listLvl.add(lvl);
					line = br.readLine();
				}
				br.close();
				fr.close();
			}
			catch (IOException exception)
			{
				System.out.println ("Erreur lors de la lecture : " + exception.getMessage());
			}
		}
		catch (FileNotFoundException exception)
		{
			System.out.println ("Le fichier n'a pas �t� trouv�");
		}

		return listLvl;
	}

	public static int dice(int min, int max){
		int result = 0;
		long range = (long)max - (long)min +1;
		long fraction = (long)(range*Math.random());
		result = (int)(fraction + min);
		return result;
	}
	
	public static int[] randomCarac(int niveau){
		int[] carac = new int[PlayerField.CARAC_LENGTH];
		//int test = 0;
		int dispo = 0;
		int palierMin = 2;
		for(int i = 2; i <= niveau ; i++){
			dispo += i + 8;
		}
		for(int i = 0; i < 8; i++){
			carac[i] = 5 ;
		}
		//System.out.println("avant : " + dispo);
		while(dispo > palierMin*8){
			int dispo2 = (int)Math.floor((double)dispo/(double)8);
			//System.out.println(dispo2);
			int random = 0;
			for(int i = 0; i < 8; i++){
				random = dice(0 , dispo2);
				carac[i] = calculReste(carac[i], random)[0];
				dispo = dispo - calculReste(carac[i], random)[1];
				//System.out.println(dispo);
				if(palierMin < calculReste(carac[i], random)[2]){
					palierMin = calculReste(carac[i], random)[2];
				}
			}
		}
		//System.out.println("apres : " + dispo + " " + palierMin);
		for(int i = 0; dispo >= palierMin ; i++){
			int random = dice(0 , dispo);
			int random2 = dice(0, 7);
			carac[random2] = calculReste(carac[random2], random)[0];
			//System.out.println(dispo);
			dispo = dispo - calculReste(carac[i], random)[1];
			if(palierMin < calculReste(carac[i], random)[2]){
				palierMin = calculReste(carac[i], random)[2];
			}
		}
		carac[PlayerField.TAIJUTSU] = caracSecondaire(carac[PlayerField.FORCE] , carac[PlayerField.AGILITE]);
		carac[PlayerField.NINJUTSU] = caracSecondaire(carac[PlayerField.SAGESSE] , carac[PlayerField.DEXTERITE]);
		carac[PlayerField.GENJUTSU] = caracSecondaire(carac[PlayerField.INTELLIGENCE] , carac[PlayerField.SAGESSE]);
		carac[PlayerField.KENJUTSU] = caracSecondaire(carac[PlayerField.AGILITE] , carac[PlayerField.DEXTERITE]);
		carac[PlayerField.EISEI] = caracSecondaire(carac[PlayerField.VITALITE] , carac[PlayerField.INTELLIGENCE]);
		carac[PlayerField.GEKA] = caracSecondaire(carac[PlayerField.INTELLIGENCE] , carac[PlayerField.DEXTERITE]);
		carac[PlayerField.HIHEI] = caracSecondaire(carac[PlayerField.INTELLIGENCE] , carac[PlayerField.FORCE]);
		carac[PlayerField.CRITIQUE] = 5 + caracTertiaire(carac[PlayerField.DEXTERITE]);
		carac[PlayerField.EVASION] = 5 + caracTertiaire(carac[PlayerField.AGILITE]);
		carac[PlayerField.OSMOSE] = 5 + caracTertiaire(carac[PlayerField.SAGESSE]);
		carac[PlayerField.MALICE] = caracTertiaire(carac[PlayerField.INTELLIGENCE]);
		carac[PlayerField.HP] = calculHpMp(carac[PlayerField.EPH], niveau);
		carac[PlayerField.MP] = calculHpMp(carac[PlayerField.EMP], niveau);
		carac[PlayerField.NIVEAU] = niveau;

		
		return carac;
	}
	
	/*public static int[] calculReste(int carac, int pt){
		int reste = pt;
		int palier = 2;
		int limite = 11;
		int carac2 = carac;
		while(reste >= palier){
			if(carac2 >= limite){
				limite += 10;
				palier++;
			}
			else{
				reste = pt - ((limite - carac)*palier);
				carac2 = (limite - carac);
			}
		}
		return new int[]{carac2,pt - reste, palier};
	}*/
	
	public static int[] calculReste(int carac, int pt){
		int reste = pt;
		int palier = 2;
		int limite = 11;
		int carac2 = carac;
		int result = 0;
		while(reste >= palier){
			if(carac2 >= limite){
				limite += 10;
				palier++;
			}
			else{
				if((reste - ((limite - carac2)*palier)) >= 0){
					reste = reste - ((limite - carac2)*palier);
					result += (limite - carac2)*palier;
					carac2 += (limite - carac2);
				}
				else{
					reste = reste - (reste % palier);
					carac2 += Math.floor((double)reste/(double)palier);
					result += reste;
					return new int[]{carac2,result, palier};
				}
			}
		}
		//System.out.println(" result : " + result + " carac2 : " + carac2 + " carac : " + carac + " pt : " + pt);
		return new int[]{carac2,result, palier};
	}
	
	public static int caracSecondaire(int carac1 ,int carac2){
		return (int)Math.ceil((double)(carac1 + carac2)/2);
	}
	public static int caracTertiaire(int carac){
		return ((carac - 1)/10)*5;
	}
	
	public static int calculHpMp(int carac, int niveau){
		int result = 0;
		result = 100 + 20*carac;
		for(int i = 2; i <= niveau; i++){
			result += (i + 8)*2;
		}
		return result;
	}
	
	public static int[] loadBuild(String buildName){
		ArrayList<int[]> tab = new ArrayList<int[]>();
		try{
			File f = new File("");
			if(MainSystem.IN_TESTING)f = new File("signature/" + buildName);
			else f = new File("./signature/" + buildName);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader (fr);
			try{
				String line = br.readLine();
				while(line != null){
					if(line.startsWith("%V")){
						Pattern pattern = Pattern.compile("\\d+");
						Matcher matcher = pattern.matcher(line);
						int i = 0;
						int[] level = new int[Fields.NUM_CARAC];
						while(matcher.find()){
							level[i] = Integer.parseInt(matcher.group());
							i++;
						}
						tab.add(level);
					}
					line = br.readLine();
				}
			}
			catch (IOException exception)
			{
				System.out.println ("Erreur lors de la lecture : " + exception.getMessage());
			}
		}
		catch (FileNotFoundException exception)
		{
			System.out.println ("Le fichier n'a pas �t� trouv�");
		}
		int[] result = new int[8];
		for(int i = 0; i < result.length ; i++){
			result[i] = tab.get(tab.size()-1)[i];
		}
		
		return tab.get(tab.size()-1);
	}
	
	public static int[] calculCarac(int[] value){
		int[] value2 = new int[8];
		int[] carac = new int[PlayerField.CARAC_LENGTH];
		for(int i = 0; i < value2.length ; i++){
			value2[i] = value[i];
			carac[i] = value[i];
		}
		
		carac[PlayerField.TAIJUTSU] = caracSecondaire(carac[PlayerField.FORCE] , carac[PlayerField.AGILITE]);
		carac[PlayerField.NINJUTSU] = caracSecondaire(carac[PlayerField.SAGESSE] , carac[PlayerField.DEXTERITE]);
		carac[PlayerField.GENJUTSU] = caracSecondaire(carac[PlayerField.INTELLIGENCE] , carac[PlayerField.SAGESSE]);
		carac[PlayerField.KENJUTSU] = caracSecondaire(carac[PlayerField.AGILITE] , carac[PlayerField.DEXTERITE]);
		carac[PlayerField.EISEI] = caracSecondaire(carac[PlayerField.VITALITE] , carac[PlayerField.INTELLIGENCE]);
		carac[PlayerField.GEKA] = caracSecondaire(carac[PlayerField.INTELLIGENCE] , carac[PlayerField.DEXTERITE]);
		carac[PlayerField.HIHEI] = caracSecondaire(carac[PlayerField.INTELLIGENCE] , carac[PlayerField.FORCE]);
		carac[PlayerField.CRITIQUE] = 5 + caracTertiaire(carac[PlayerField.AGILITE]);
		carac[PlayerField.EVASION] = 5 + caracTertiaire(carac[PlayerField.DEXTERITE]);
		carac[PlayerField.OSMOSE] = 5 + caracTertiaire(carac[PlayerField.OSMOSE]);
		carac[PlayerField.MALICE] = caracTertiaire(carac[PlayerField.INTELLIGENCE]);
		carac[PlayerField.HP] = calculHpMp(carac[PlayerField.EPH], value[value.length-1]);
		carac[PlayerField.MP] = calculHpMp(carac[PlayerField.EMP], value[value.length-1]);
		carac[PlayerField.NIVEAU] = value[value.length-1];
		
		return carac;
	}
}
