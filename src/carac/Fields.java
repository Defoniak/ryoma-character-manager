package carac;

public class Fields {
	public static final int ASPIRANT = 0, GENIN = 1, CHUUNIN = 2, JUUNIN = 3, ANBU = 4, SANIN_KAGE = 5;
	
	public static final int FORCE = 0, AGILITE = 1, VITALITE = 2, DEXTERITE = 3, INTELLIGENCE = 4, SAGESSE = 5, EPH = 6, EMT = 7, PE = 8;
	public static final int TAIJUTSU = 9, NINJUTSU = 10, GENJUTSU = 11, KENJUTSU = 12, EISEI = 13, GEKA = 14, HIHEI = 15;
	public static final int EVASION = 16, CRITIQUE = 17, MALICE = 18, OSMOSE = 19;
	public static final int HP = 20, MP = 21, PR = 22, NIVEAU = 23;
	
	public static final int KATON = 0, DOTON = 1, SUITON = 2, FUTON = 3, RAITON = 4;
	
	public static final int NUM_CARAC = 24;
	
	public String getGradeName(int gradeCode){
		switch(gradeCode){
		case ASPIRANT: return "aspirant";
		case GENIN: return "genin";
		case CHUUNIN: return "chuunin";
		case JUUNIN: return "juunin";
		case ANBU: return "anbu";
		case SANIN_KAGE: return "sanin/kage";
		default: return "";
		}
	}
	
	public static String getCaracName(int caracCode){
		String[] caracName = {"force : ", "agilite : ", "vitalite : ", "dexterite : ", "intelligence : ", "sagesse : ", "EPH : ", "EMT : ", "points excedentaires : ",
				"taijutsu : ", "ninjutsu : ", "genjutsu : ", "kenjutsu : ", "eisei : ", "geka : ", "hihei : ", "evasion : ", "critique : ", "malice : ", "osmose : ", 
				"hp : ", "mp : ", "points restants : ", "niveau : "};
		
		return caracName[caracCode];
	}
}
