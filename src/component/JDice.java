package component;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import carac.Utilitaires;

public class JDice extends JPanel implements ActionListener{
	
	JLabel nbreMinL = new JLabel("Nombre minimum : ");
	JLabel nbreMaxL = new JLabel("Nombre maximum : ");
	JLabel resultL = new JLabel("Resultat : ");
	JButton bouton = new JButton("Generer");
	
	JTextField nbreMinT = new JTextField("0");
	JTextField nbreMaxT = new JTextField("0");
	
	public JDice(){
		super();
		this.setBorder(BorderFactory.createTitledBorder("Dice"));
		JPanel main = new JPanel();
		main.setLayout(new GridLayout(3,2));
		add(main);
		bouton.addActionListener(this);
		this.setVisible(true);
		
		main.add(nbreMinL);
		main.add(nbreMinT);
		main.add(nbreMaxL);
		main.add(nbreMaxL);
		main.add(nbreMaxT);
		main.add(resultL);
		main.add(bouton);
	}
	
	/*public int dice(int min, int max){
		int result = 0;
		long range = (long)max - (long)min +1;
		long fraction = (long)(range*Math.random());
		result = (int)(fraction + min);
		return result;
	}*/
	
	public void actionPerformed(ActionEvent e){
		JButton bouton = (JButton)e.getSource();
		if(bouton == this.bouton){
			String result = "";
			try{
				if(Integer.parseInt(nbreMinT.getText()) < Integer.parseInt(nbreMaxT.getText())){
					//result ="" +  dice(Integer.parseInt(nbreMinT.getText()),Integer.parseInt(nbreMaxT.getText()));
					result ="" +  Utilitaires.dice(Integer.parseInt(nbreMinT.getText()),Integer.parseInt(nbreMaxT.getText()));
				}
				else{
					//JOptionPane jop = new JOptionPane();
					JOptionPane.showMessageDialog(null, "Tu as pens� � mettre un nombre maximum plus grand que le nombre minimum ?", "Erreur 42", JOptionPane.ERROR_MESSAGE);
				}
			}
			catch(NumberFormatException nfe){
				//JOptionPane jop = new JOptionPane();
				JOptionPane.showMessageDialog(null, "N'importe quoi >< essaye d'entrer des chiffres ca marchera peut �tre mieux !", "Erreur 132", JOptionPane.ERROR_MESSAGE);
			}
			resultL.setText("Resultat : " + result);
		}
	}
	
}
