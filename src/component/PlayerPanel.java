package component;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import carac.Player;
import carac.PlayerField;
import carac.Technique;
import carac.Weapon;

import view.EditorFrame;

public class PlayerPanel extends JPanel implements ActionListener{
	
	private Player player = new Player();
	private ArrayList<Technique> dataTech = new ArrayList<Technique>();
	private ArrayList<Weapon> dataWeapon = new ArrayList<Weapon>();

	
	JLabel name = new JLabel("Nom : Unknown           ");
	JLabel grade = new JLabel("Grade : Aspirant              ");
	JButton edit = new JButton("Editer");
	JLabel kido = new JLabel("");
	
	CaracPanel caracPanel= new CaracPanel(player.getCarac());
	TechniquePanel techniquePanel = new TechniquePanel(player.getTechList());
	WeaponPanel weaponPanel = new WeaponPanel();
	BonusMalusPanel  bonusMalusPanel = new BonusMalusPanel("");
	
	
	//private PlayerEditorPanel playerEditorPanel = new PlayerEditorPanel(this);
	
	
	public PlayerPanel(){
		super();
		this.setBorder(BorderFactory.createTitledBorder("Fiche joueur"));
		edit.addActionListener(this);
		
		JPanel main = new JPanel();
		
		JPanel mid = new JPanel();
		JPanel top = new JPanel();
		JPanel bot = new JPanel();
		
		JPanel flowTop = new JPanel();
		flowTop.setLayout(new FlowLayout());
		
		JPanel gridBot = new JPanel();
		gridBot.setLayout(new GridLayout(1,2));
		
		
		JPanel right = new JPanel();
		weaponPanel.setLayout(new BoxLayout(weaponPanel, BoxLayout.LINE_AXIS));
		bonusMalusPanel.setLayout(new BoxLayout(bonusMalusPanel, BoxLayout.LINE_AXIS));
		right.setLayout(new BoxLayout(right, BoxLayout.PAGE_AXIS));
		right.add(weaponPanel);
		right.add(bonusMalusPanel);
		
		top.setLayout(new BoxLayout(top, BoxLayout.LINE_AXIS));
		mid.setLayout(new BoxLayout(mid, BoxLayout.LINE_AXIS));
		bot.setLayout(new BoxLayout(bot, BoxLayout.LINE_AXIS));
		
		top.add(flowTop);
		flowTop.add(name);
		flowTop.add(grade);
		flowTop.add(edit);
		flowTop.add(kido);
		
		mid.add(caracPanel);
		
		bot.add(gridBot);
		gridBot.add(techniquePanel);
		gridBot.add(right);
		

		main.add(top);
		main.add(mid);
		main.add(bot);
		main.setLayout(new BoxLayout(main, BoxLayout.PAGE_AXIS));
		
		this.add(main);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton)e.getSource();
		if(button == edit){
			PlayerEditorPanel playerEditorPanel = new PlayerEditorPanel(this);
			EditorFrame editor = new EditorFrame(playerEditorPanel);
		}
		
	}

/*	public PlayerEditorPanel getPlayerEditorPanel() {
		return playerEditorPanel;
	}

	public void setPlayerEditorPanel(PlayerEditorPanel playerEditorPanel) {
		this.playerEditorPanel = playerEditorPanel;
	}*/

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
		//System.out.println(player.getTechList().size());
		caracPanel.setCarac(player.getCarac());
		techniquePanel.setTechList(player.getTechList());
		bonusMalusPanel.setText(player.getBonus());
		weaponPanel.setArmor(player.getArmor(), player.getArmorQuality());
		weaponPanel.setWeaponList(player.getWeaponList());
		name.setText("Nom : " + player.getName() + "           ");
		grade.setText("Grade : " + PlayerField.getRankName(player.getGrade()) + "              ");
		//kido.setText((player.isKido())?"        Adepte du Kido":"");
	}
	public ArrayList<Technique> getDataTech(){
		return dataTech ;
	}
	public ArrayList<Weapon> getDataWeapon(){
		return dataWeapon ;
	}
	public void setDataTech(ArrayList<Technique> dataTech){
		this.dataTech = dataTech;
	}
	public void setDataWeapon(ArrayList<Weapon> dataWeapon){
		this.dataWeapon = dataWeapon;
	}
}
