package component;

import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import carac.PlayerField;

public class CaracPanel extends JPanel{
	
	private ArrayList<JLabel> labelList = new ArrayList<JLabel>();
	
	public CaracPanel(int[] carac){
		super();
		this.setBorder(BorderFactory.createTitledBorder("Caracteristiques"));
		JPanel main = new JPanel();
		main.setLayout(new GridLayout(3,7));
		
		
		for(int i = 0 ; i < PlayerField.CARAC_LENGTH ; i++){
			JLabel label;
			label = new JLabel(PlayerField.getCaracName(i) + carac[i] + " ");
			Font font = new Font("blop",Font.ROMAN_BASELINE,12);
			label.setFont(font);
			labelList.add(label);
			main.add(labelList.get(i));
		}
		/*if(kido == true){
			labelList.get(PlayerField.MP).setText(PlayerField.getCaracName(PlayerField.MP) + (carac[PlayerField.MP])/2 + " ");
		}*/
		
		this.add(main);
	}
	
	public void setCarac(int[] carac) {
		int i = 0 ;
		for(JLabel label : labelList){
			label.setText(PlayerField.getCaracName(i) + carac[i] + " ");
			i++;
		}
		/*if(kido == true){
			labelList.get(PlayerField.MP).setText(PlayerField.getCaracName(PlayerField.MP) + (carac[PlayerField.MP])/2 + " ");
		}*/
	}
}
