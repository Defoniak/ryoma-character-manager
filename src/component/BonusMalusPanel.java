package component;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class BonusMalusPanel extends JPanel{

	private JTextArea text = new JTextArea();
	
	public BonusMalusPanel(String string){
		super();
		this.setBorder(BorderFactory.createTitledBorder("Bonus/Malus"));
		
		getText().setEditable(false);
		getText().setText(string);
		getText().setLineWrap(true);
		JScrollPane listScroller = new JScrollPane(getText());
		listScroller.setPreferredSize(new Dimension(200, 150));
		this.add(listScroller);
	}

	public JTextArea getText() {
		return text;
	}

	public void setText(String text) {
		this.text.setText(text);
	}
	
	
	
}
