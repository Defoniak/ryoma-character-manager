package component;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.html.HTMLEditorKit;

import carac.PlayerField;
import carac.Technique;

public class TechniquePanel extends JPanel implements ItemListener{

	JComboBox techBox = new JComboBox();
	JEditorPane techPane = new JEditorPane();
	private ArrayList<Technique> techList = new ArrayList<Technique>();
	boolean isEdition = false ;

	public TechniquePanel(ArrayList<Technique> techList){
		super();
		this.setBorder(BorderFactory.createTitledBorder("Techniques"));
		techBox.addItemListener(this);
		
		//this.setTechList(techList);
		for(Technique tech : techList){
			techBox.addItem(tech);
		}

		techPane.setEditable(false);
		techPane.setEditorKit(new HTMLEditorKit());
		JScrollPane listScroller = new JScrollPane(techPane);
		listScroller.setPreferredSize(new Dimension(300, 450));
		listScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		techPane.setMargin(new Insets(5, 5, 5, 15));
		JPanel bot = new JPanel();
		JPanel top = new JPanel();
		JPanel main = new JPanel();
		//main.setLayout(new GridLayout(2,1));
		top.setLayout(new BoxLayout(top,BoxLayout.LINE_AXIS));
		bot.setLayout(new BoxLayout(bot,BoxLayout.LINE_AXIS));
		main.setLayout(new BoxLayout(main, BoxLayout.PAGE_AXIS));
		top.add(techBox);
		bot.add(listScroller);
		main.add(top);
		main.add(bot);
		this.add(main);
	}

	/*private String toMegaString(Technique tech){
		//tech.setName("Le grand souffle du dragon");
		//tech.setText("<strong><font color=\"#cc3300\">Karyuudan Endan � Le Grand Souffle du Dragon de Feu</font><br /><font color=\"crimson\">Technique de rang C</font><br /><font color=\"darkgoldenrod\">Arcane</font><br /><font color=\"green\">Utilisation � distance et au corps � corps</font><br /><font color=\"#E29107\">Niveau minimum : 9<br />Ninjutsu requis : 18<br />Technique requise : Katon</font></strong><br /><br />Un large jet de flammes denses quitte votre bouche, et s'�lance en direction de votre cible.<br /><i>=> Pour 20 MP, 12 d�g�ts. Si votre DEX est sup�rieure � l'AGI adverse, vous parvenez � viser une partie sp�cifique du corps de votre cible.</i><br /><strong>C�ur ardent :</strong><br />NIN requis : 25 � Pour <i>15 MP</i>, vous ignorez l�armure adverse.<br /><strong><font color=\"purple\">D�g�ts</strong></font><br /><strong>Aspirant</strong> : <i>20 MP</i>, 15 d�g�ts.<br /><strong>Genin</strong> : <i>30 MP</i>, 25 d�g�ts.<br /><strong>Chuunin</strong> : <i>45 MP</i>, 40 d�g�ts.<br /><strong>Juunin</strong> : <i>55 MP</i>, 50 d�g�ts.<br /><strong>Anbu</strong> : <i>70 MP</i>, 60 d�g�ts<br /><strong>Sannin - Kage</strong> : <i>85 MP</i>, 75 d�g�ts.<br /><strong><font color=\"purple\">Partie du Corps</strong></font><br /><strong>Aspirant - Genin</strong> : Main. Votre cible a du mal � manipuler ses armes, elle perd 5 FOR et 5% en Critique, cumulable jusqu�� deux fois, pour une dur�e de 4 actions.<br /><strong>Chuunin</strong> : Genou. Votre adversaire perd 10 % en Evasion et ne pourra plus d�clarer d�esquive durant 4 actions. <br /><strong>Juunin - Anbu</strong> : T�te. Votre adversaire perd �5% en Eva et 15 % en Critique pour 4 actions. De plus sa prochaine action est obligatoirement une action d�fensive. Si jamais il avait d�clar� une action offensive, cette derni�re est d�cal�e � l�action suivante et � la place l�adversaire d�clare une Esquive. Si l�action offensive ne peut �tre lanc�e durant le tour, elle sera lanc�e au tour suivant, en premi�re action.<br /><strong>Sannin - Kage</strong> : Torse. Votre adversaire perd un point d�action lors de sa prochaine d�claration.<br /><strong>Calcul des d�g�ts finaux :</strong> (NIN + D�g�ts de l�Arcane) � (VIT + ARM)(<i>adverse</i>)<br /><font color=\"blue\">Les effets sont cumulables, mais jamais entre eux (exception pour la main, dont les malus peuvent �tre combin� deux fois). Vous pouvez lancer la technique � des grades diff�rents. Par exemple un Sannin peut d�cider d'infliger 75 d�g�ts (Sannin), mais de viser la main (Genin). L'inverse n'est pas possible, vous devez  avoir le grade requis (ou plus) dans les deux cas.<br /><u>Note</u> : On rappelle que faire des signes pour lancer une technique (offensive ou d�fensive) est une action d�fensive.</font><br /><font color=\"red\">Co�t d'activation : 3 points d'action</font>");
		/*tech.setPrerequis("Niveau requis : 4 <br /> Technique requise : Ma�trise du Ninjutsu");
		tech.setDescription("Un shinobi du Pays du Vent crut un jour avoir r�ussit une approche parfaite. Il avait frapp� son adversaire � l'abdomen et au genou, de telle sorte � l'incapaciter, puis il s'�tait recul� bien � l'abri d'une coquille de terre et d'un vent violent. Son ennemi, un shinobi de Konoha, resta calmement � terre, sans forcer sur sa jambe blesser. Il regardait le ciel nuageux et �couta d'une oreille distraite la terrible d�tonation qui �branla la barri�re en terre. Quand il baissa les yeux, celle-ci �tait effondr�e, recouvrant ce qui restait de l'homme du Pays du Vent.");
		tech.setExemple("=> Pour 10 MP, d�s que votre cible est touch�e par une attaque, elle subit 10 d�g�ts non-pr�venables suppl�mentaires.");
		tech.setEffets("C�ur ardent : NIN requis : 30 � Pour 20 MP, vous d�calez l�explosion d�une action. Cumulable. NIN requis : 32 � Pour 20 MP, d�g�ts augment�s de 20. NIN requis : 50 � Pour 100 MP, d�g�ts augment�s de 160. => Pour 30 MP, vous posez une mine. Celle-ci causera 50 d�g�ts dans 4 actions, ou lorsque vous aurez pay� 5 MP. Aspirant - Genin : 40 MP, 50 d�g�ts dans 4 actions, ou lorsque vous aurez pay� 5 MP. Chuunin : 60 MP, 80 d�g�ts dans 4 actions, ou lorsque vous aurez pay� 10 MP. Juunin � Anbu : 80 MP, 110 d�g�ts dans 4 actions, ou lorsque vous aurez pay� 15 MP. Sannin - Kage : 100 MP, 140 d�g�ts dans 4 actions, ou lorsque vous aurez pay� 20 MP. Calcul des d�g�ts finaux : (D�g�ts de l�arcane) � (VIT)(adverse)");
		tech.setBlue("Le d�compte commence l�action suivant la pose de la mine, l�explosion survient avant toute autre action. La mine est pos�e sur la cible vis�e, par cons�quent, elle suivra ses mouvements et il n'existe aucun moyen de la retirer.");
		tech.setPa("coute 3 pa");
		techPane.setText("<!DOCTYPE html><html><head> <meta charset=\"utf-8\" /><title>Titre</title> " +
				"</head> <body><b TEXT=\"#CC3300\">" + tech.getName() +
				"</b> <br /><b TEXT=\"#DC143C\">" + PlayerField.getGradeName(tech.getGrade()) + 
				"</b> <br /><b TEXT=\"" + getColor(tech.getType()) + "\">" + PlayerField.getTypeName(tech.getType()) + 
				"</b> <br /><b TEXT=\"#008000\">" + PlayerField.getRangeName(tech.getRange()) + 
				"</b><br /><b TEXT=\"#E29107\">" + tech.getPrerequis() + 
				"</b><p>" + tech.getDescription() + "<i><br />" + tech.getExemple() + "</i><br />" + toPrettyText( tech.getEffets()) + 
				"<div TEXT=\"#0000FF\">" + tech.getBlue() + "</div><div TEXT=\"#FF0000\">" + tech.getPa() + "</div></p></body></html>");
		
		techPane.setText(toPrettyText(tech.getText()));
		return "";
	}*/

	/*private String getColor(int typeCode){
		String result = "";
		switch(typeCode){
		case PlayerField.ARCANE: result = "#BD860B";
		break;
		case PlayerField.SPECIALE: result = "#FF4500";
		break;
		case PlayerField.ILLUSION: result = "#1E90FF";
		break;
		case PlayerField.PHYSIQUE: result = "#4B0082";
		break;
		case PlayerField.COMPLEMENTAIRE: result = "#2E8B6C";
		break;
		case PlayerField.INVOCATION: result = "#9932CC";
		break;
		default: result = "#000000";
		}
		return result;
	}*/
	
	private String toPrettyText(String text){
		String result = text;
		/*for(int i = 0; i < PlayerField.RANK_LENGHT ; i++){
			result = result.replaceAll(PlayerField.getRankName(i),"<b>" + PlayerField.getRankName(i) + "</b>");
		}
		result = result.replaceAll("Calcul des d�g�ts finaux","<b>Calcul des d�g�ts finaux</b>");
		result = result.replaceAll("C�ur ardent","<b>C�ur ardent</b>");*/
		//System.out.println(result);
		result = result.replaceAll("crimson", "#DC143C");
		result = result.replaceAll("darkgoldenrod", "#B8860B");
		result = result.replaceAll("indigo", "#4B0082");
		result = result.replaceAll("dodgerblue", "#1E90FF");
		result = result.replaceAll("seagreen", "#2E8B57");
		result = result.replaceAll("darkorchid", "#9932CC");
		result = result.replaceAll("orangered", "#FF4500");
		//System.out.println("blop " + result);
		return result;
	}
	
	@Override
	public void itemStateChanged(ItemEvent e) {
		JComboBox box = (JComboBox)e.getSource();
		if(box == techBox && ! isEdition){
			
			System.out.println(techBox.getItemCount() + " " + techList.size());
			Technique tech = (Technique)techBox.getSelectedItem();
			techPane.setText(toPrettyText(tech.getText()));
		}
	}

	public ArrayList<Technique> getTechList() {
		return techList;
	}

	public void setTechList(ArrayList<Technique> techList) {
		isEdition = true ;
		this.techList = techList;
		techBox.removeAllItems();
		for(Technique tech : techList){
			techBox.addItem(tech);
		}
		isEdition = false ;
		if(techBox.getItemCount() > 0){
			System.out.println("blop");
			techPane.setText(toPrettyText(((Technique)(techBox.getItemAt(0))).getText()));
		}
		else{
			techPane.setText("");
		}
	}
}
