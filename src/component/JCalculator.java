package component;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import carac.Player;
import carac.PlayerField;
import carac.Utilitaires;
import carac.Weapon;

public class JCalculator extends JPanel implements ActionListener, ItemListener{

	JButton result = new JButton("Calculer");

	JLabel label1 = new JLabel("attaque avec ");
	JLabel label2 = new JLabel(" gr�ce � un bonus de ");
	JLabel label3 = new JLabel("% en critique sur ");
	JLabel label4 = new JLabel(" qui a un bonus d'evasion de ");
	JLabel label5 = new JLabel("%.");
	JLabel label6 = new JLabel("R�sultat : ");

	JComboBox player1Box = new JComboBox();
	JComboBox player2Box = new JComboBox();
	JComboBox weaponBox = new JComboBox();

	JTextField critique = new JTextField("0");
	JTextField evasion = new JTextField("0");

	private ArrayList<Player> playerList;


	public JCalculator(ArrayList<Player> playerList){
		super();
		this.setBorder(BorderFactory.createTitledBorder("Calculotron"));

		//this.setPlayerList(playerList);
		for(Player player : playerList){
			player1Box.addItem(player);
			player2Box.addItem(player);
		}

		for(Weapon weapon : playerList.get(0).getWeaponList()){
			weaponBox.addItem(weapon);
		}
		Weapon poing = new Weapon("Poing", 5);
		poing.setQuality(-1);
		weaponBox.addItem(poing);

		result.addActionListener(this);
		player1Box.addItemListener(this);
		critique.setColumns(2);
		evasion.setColumns(2);

		JPanel main = new JPanel();
		main.setLayout(new GridLayout(2,1));
		add(main);

		JPanel top = new JPanel();
		//top.setLayout(new GridLayout(1,10));
		top.setLayout(new FlowLayout());
		main.add(top);

		JPanel bot = new JPanel();
		//bot.setLayout(new GridLayout(1,2));
		bot.setLayout(new FlowLayout());
		main.add(bot);

		top.add(player1Box);
		top.add(label1);
		top.add(weaponBox);
		top.add(label2);
		top.add(critique);
		top.add(label3);
		top.add(player2Box);
		top.add(label4);
		top.add(evasion);
		top.add(label5);

		bot.add(result);
		bot.add(label6);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		JButton button =(JButton)e.getSource();
		if(button == result){
			label6.setText("Resultat : ");
			Player player1 = ((Player)player1Box.getSelectedItem());
			Player player2 = ((Player)player2Box.getSelectedItem());
			try{
				int critiqueTotale = player1.getCarac(PlayerField.CRITIQUE) + Integer.parseInt(critique.getText());
				int esquiveTotale = player2.getCarac(PlayerField.EVASION) + Integer.parseInt(evasion.getText());
				int multiplicator = 1;
				int damage = 0;
				String blop = "";
				if(esquiveTotale + Utilitaires.dice(1, 100) < 100){ // esquive rat�e
					if(critiqueTotale + Utilitaires.dice(1, 100) >= 100){  // critique r�ussi
						multiplicator = 3;
						blop = " effectue un coup critique sur ";
					}
					else{ // critique rat�
						blop = " touche ";
					}
					damage = player1.getCarac(PlayerField.FORCE) + ((((Weapon)weaponBox.getSelectedItem()).getRealAtk())*multiplicator) - player2.getCarac(PlayerField.VITALITE) - player2.getTotalArmor();
					if(damage < 0) damage = 0;
					label6.setText(label6.getText() + player1.getName() + blop + player2.getName() + " pour " + damage + " points de degat.");
				}
				else{ // esquive r�ussie
					label6.setText(label6.getText() + player2.getName() + " a esquiv� le coup de " + player1.getName());
				}
			}
			catch(NumberFormatException nfe){
				JOptionPane.showMessageDialog(null, 
						"essaye d'entrer des chiffres ca marchera peut �tre mieux !",
						"Erreur 132", 
						JOptionPane.ERROR_MESSAGE);
			}
		}

	}


	@Override
	public void itemStateChanged(ItemEvent e) {
		JComboBox box =(JComboBox)e.getSource();
		if(box == player1Box){
			weaponBox.removeAllItems();
			for(Weapon weapon : ((Player)player1Box.getSelectedItem()).getWeaponList()){
				weaponBox.addItem(weapon);
			}
			weaponBox.addItem(new Weapon("Poing", 5).getName());
		}

	}


	public ArrayList<Player> getPlayerList() {
		return playerList;
	}
}
