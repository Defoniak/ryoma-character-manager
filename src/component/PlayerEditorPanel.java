package component;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import view.RefereeFrame;

import carac.Player;
import carac.PlayerField;
import carac.Technique;
import carac.Utilitaires;
import carac.Weapon;

public class PlayerEditorPanel extends JPanel implements ActionListener, KeyListener{

	PlayerPanel playerPanel;

	JLabel lName = new JLabel("Nom : ");
	JLabel lGrade = new JLabel("Grade : ");
	JTextField tName = new JTextField();
	JComboBox gradeBox = new JComboBox();

	ArrayList<JLabel> labelList = new ArrayList<JLabel>();
	ArrayList<JTextField> textList = new ArrayList<JTextField>();

	JLabel lSearch = new JLabel("Search : ");
	JTextField tSearch = new JTextField("");
	JButton bSearch = new JButton("Go !");

	JComboBox weaponBox = new JComboBox();
	JComboBox weaponQualityBox = new JComboBox();
	JButton addWeapon = new JButton("+");

	JComboBox armorBox = new JComboBox();
	JComboBox armorQualityBox = new JComboBox();

	JButton confirm = new JButton("Valider les modifications");
	JButton confirmAndRegister = new JButton("Enregistrer et valider les modifications");
	JButton cancel = new JButton("Annuler les modifications");

	JButton addTech = new JButton("=>");
	JTextArea bonus = new JTextArea();

	DefaultListModel dataModel = new DefaultListModel();
	DefaultListModel techModel = new DefaultListModel();
	DefaultListModel weaponModel = new DefaultListModel();
	JList dataBase = new JList(dataModel);
	JList techList = new JList(techModel);
	JList listWeapon = new JList(weaponModel);

	ArrayList<Weapon> weaponList = new ArrayList<Weapon>();
	ArrayList<Technique> listTech = new ArrayList<Technique>();

	JButton random = new JButton("Generer un pnj aleatoire");

	//player
	private Player player = new Player();
	ArrayList<Weapon> playerListWeapon = new ArrayList<Weapon>();
	ArrayList<Technique> playerListTech = new ArrayList<Technique>();
	//


	public PlayerEditorPanel(PlayerPanel playerPanel){	
		super();
		this.setBorder(BorderFactory.createTitledBorder("mode �dition"));

		this.playerPanel = playerPanel ;
		addTech.addActionListener(this);
		bSearch.addActionListener(this);
		addWeapon.addActionListener(this);
		confirm.addActionListener(this);
		confirmAndRegister.addActionListener(this);
		cancel.addActionListener(this);
		dataBase.addMouseListener(new PopClickListener());
		//dataBase.get.addMouseListener(new PopClickListener());
		techList.addMouseListener(new PopClickListener());
		listWeapon.addMouseListener(new PopClickListener());
		tSearch.addKeyListener(this);
		random.addActionListener(this);


		tName.setColumns(15);
		tSearch.setColumns(20);

		JPanel gridTop = new JPanel();
		gridTop.setLayout(new GridLayout(2,1));	
		JPanel gridTop1 = new JPanel();
		JPanel gridTop2 = new JPanel();
		gridTop.add(gridTop1);
		gridTop.add(gridTop2);
		for(int i = 0 ; i < PlayerField.CARAC_LENGTH ; i++){
			JLabel label = new JLabel(" " + PlayerField.getCaracName(i));
			JTextField text = new JTextField("0");
			text.setColumns(3);
			Font font = new Font("blop",Font.ROMAN_BASELINE,12);
			label.setFont(font);
			labelList.add(label);
			textList.add(text);
			if(i <= PlayerField.CARAC_LENGTH/2 ){
				gridTop1.add(labelList.get(i));
				gridTop1.add(textList.get(i));
			}
			else{
				gridTop2.add(labelList.get(i));
				gridTop2.add(textList.get(i));
			}
		}

		for(int i = 0; i < PlayerField.RANK_LENGHT ; i++){
			gradeBox.addItem(PlayerField.getRankName(i));
		}
		for(int i = 0; i < PlayerField.QUALITY_LENGTH ; i++){
			armorQualityBox.addItem(PlayerField.getQualityName(i));
			weaponQualityBox.addItem(PlayerField.getQualityName(i));
		}
		for(int i = 0; i < PlayerField.RANK_LENGHT ; i++){
			armorBox.addItem(PlayerField.getArmorName(i));
		}
		JPanel gridBot = new JPanel();
		//gridBot.setLayout(new GridLayout(1,2));

		JPanel flowHead = new JPanel();
		flowHead.setLayout(new FlowLayout());

		JScrollPane dataBaseScroller = new JScrollPane(dataBase);
		dataBaseScroller.setPreferredSize(new Dimension(300, 230));
		dataBaseScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		JScrollPane techScroller = new JScrollPane(techList);
		techScroller.setPreferredSize(new Dimension(300, 230));
		techScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		JPanel left = new JPanel();
		left.setBorder(BorderFactory.createTitledBorder("Techniques"));
		left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));
		JPanel leftTop = new JPanel();
		JPanel flowLeft = new JPanel();
		leftTop.add(flowLeft);
		leftTop.setLayout(new BoxLayout(leftTop,BoxLayout.LINE_AXIS));	

		JPanel leftBot = new JPanel();
		JPanel gridLeft = new JPanel();
		JLabel lDataBase = new JLabel("banque de technique :                                          ");
		JLabel lTech = new JLabel("                        techniques possedees :");
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.LINE_AXIS));
		labelPanel.add(lDataBase);
		//labelPanel.add(addTech);
		labelPanel.add(lTech);
		gridLeft.setLayout(new GridLayout(1,2));
		gridLeft.add(dataBaseScroller);
		gridLeft.add(techScroller);

		leftBot.add(gridLeft);
		leftBot.setLayout(new BoxLayout(leftBot, BoxLayout.LINE_AXIS));

		JPanel right = new JPanel();
		right.setLayout(new BoxLayout(right, BoxLayout.PAGE_AXIS));
		JPanel flowRightTop = new JPanel();
		flowRightTop.setLayout(new FlowLayout());
		JPanel rightTop = new JPanel();
		rightTop.add(flowRightTop);
		rightTop.setLayout(new BoxLayout(rightTop,BoxLayout.LINE_AXIS));
		JPanel flowRightBot = new JPanel();
		JPanel rightBot = new JPanel();
		flowRightBot.setLayout(new FlowLayout());
		rightBot.add(flowRightBot);
		rightBot.setLayout(new BoxLayout(rightBot,BoxLayout.LINE_AXIS));	
		JScrollPane weaponScroller = new JScrollPane(listWeapon);
		weaponScroller.setPreferredSize(new Dimension(400, 100));
		weaponScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		JPanel weaponPanel = new JPanel();
		weaponPanel.add(weaponScroller);
		weaponPanel.setLayout(new BoxLayout(weaponPanel, BoxLayout.LINE_AXIS));
		JPanel bonusPanel = new JPanel();
		Font font = new Font(null, 0, 11);
		bonus.setFont(font);
		bonus.setLineWrap(true);
		JScrollPane bonusScroller = new JScrollPane(bonus);
		bonusScroller.setPreferredSize(new Dimension(400, 100));
		bonusScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		bonusPanel.add(bonusScroller);
		bonusPanel.setLayout(new BoxLayout(bonusPanel, BoxLayout.LINE_AXIS));

		JPanel flowFoot = new JPanel();
		flowFoot.setLayout(new FlowLayout());

		JPanel main = new JPanel(); // contient tout
		main.setLayout(new BoxLayout(main, BoxLayout.PAGE_AXIS));

		JPanel head = new JPanel();
		head.setLayout(new BoxLayout(head, BoxLayout.LINE_AXIS));
		JPanel top = new JPanel();
		top.setLayout(new BoxLayout(top, BoxLayout.LINE_AXIS));
		top.setBorder(BorderFactory.createTitledBorder("Caracteristiques"));
		JPanel bot = new JPanel();
		bot.setLayout(new BoxLayout(bot, BoxLayout.LINE_AXIS));
		JPanel foot = new JPanel();
		foot.setLayout(new BoxLayout(foot, BoxLayout.LINE_AXIS));

		JPanel superWeaponPanel= new JPanel();
		superWeaponPanel.setLayout(new BoxLayout(superWeaponPanel, BoxLayout.LINE_AXIS));

		JPanel undoSuperWeaponPanel = new JPanel();
		undoSuperWeaponPanel.setLayout(new BoxLayout(undoSuperWeaponPanel, BoxLayout.PAGE_AXIS));

		superWeaponPanel.add(undoSuperWeaponPanel);

		undoSuperWeaponPanel.add(rightTop);
		undoSuperWeaponPanel.add(weaponPanel);
		undoSuperWeaponPanel.add(rightBot);

		superWeaponPanel.add(undoSuperWeaponPanel);
		superWeaponPanel.setBorder(BorderFactory.createTitledBorder("Equipement"));
		bonusPanel.setBorder(BorderFactory.createTitledBorder("Bonus/Malus"));

		right.add(superWeaponPanel);
		right.add(bonusPanel);

		top.add(gridTop);
		bot.add(gridBot);


		head.add(flowHead);

		flowHead.add(lName);
		flowHead.add(tName);
		flowHead.add(lGrade);
		flowHead.add(gradeBox);
		flowHead.add(random);
		//flowHead.add(kido);
		main.add(head);
		main.add(top);
		main.add(bot);
		main.add(foot);

		flowLeft.add(lSearch);
		flowLeft.add(tSearch);
		//flowLeft.add(bSearch);

		left.add(leftTop);
		left.add(labelPanel);
		left.add(leftBot);

		flowRightTop.add(weaponBox);
		flowRightTop.add(weaponQualityBox);
		flowRightTop.add(addWeapon);

		flowRightBot.add(armorBox);
		flowRightBot.add(armorQualityBox);

		gridBot.add(left);
		gridBot.add(right);

		flowFoot.add(confirm);
		flowFoot.add(confirmAndRegister);
		flowFoot.add(cancel);

		foot.add(flowFoot);
		add(main);


		player = playerPanel.getPlayer();
		//kido.setSelected(player.isKido());
		weaponList = playerPanel.getDataWeapon();
		for(Weapon weapon : weaponList){
			weaponBox.addItem(weapon.getName());
		}
		listTech = playerPanel.getDataTech();
		for(Technique technique : search(tSearch.getText())){
			dataModel.addElement(technique);
		}


		tName.setText(player.getName());
		gradeBox.setSelectedIndex(player.getGrade());
		for(int i = 0; i < PlayerField.CARAC_LENGTH ; i++){
			textList.get(i).setText("" + player.getCarac()[i]);
		}
		playerListTech = player.getTechList();
		playerListWeapon = player.getWeaponList();
		for(Weapon weapon : player.getWeaponList()){
			weaponModel.addElement(weapon);
		}
		armorBox.setSelectedIndex(player.getArmor());
		armorQualityBox.setSelectedIndex(player.getArmorQuality());
		bonus.setText(player.getBonus());
		for(Technique tech : player.getTechList()){
			techModel.addElement(tech);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().getClass() == JButton.class){
			JButton bouton = (JButton)e.getSource();
			if(bouton == addWeapon){
				Weapon weapon = new Weapon();
				weapon.setName(weaponList.get(weaponBox.getSelectedIndex()).getName());
				weapon.setAtk(weaponList.get(weaponBox.getSelectedIndex()).getAtk());
				weapon.setQuality(weaponQualityBox.getSelectedIndex());
				weaponModel.addElement(weapon);
				playerListWeapon.add(weapon);
			}
			else if(bouton == addTech && dataBase.getSelectedValue() != null && notAgain((Technique)dataBase.getSelectedValue())){
				techModel.addElement((Technique)dataBase.getSelectedValue());
				playerListTech.add((Technique)dataBase.getSelectedValue());
			}
			else if(bouton == bSearch){
				dataModel.removeAllElements();
				for(Technique technique : search(tSearch.getText())){
					dataModel.addElement(technique);
				}
			}
			else if(bouton == confirm){
				if(saveCarac()){
					player.setTechList(playerListTech);
					player.setName(tName.getText());
					player.setGrade(gradeBox.getSelectedIndex());
					player.setWeaponList(playerListWeapon);
					player.setArmor(armorBox.getSelectedIndex());
					player.setAmorQuality(armorQualityBox.getSelectedIndex());
					player.setBonus(bonus.getText());
					//player.setKido(kido.isSelected());
					playerPanel.setPlayer(player);
					((RefereeFrame)(playerPanel.getTopLevelAncestor())).refresh();
					((JFrame)(((JButton)e.getSource()).getTopLevelAncestor())).dispose();
				}
			}
			else if(bouton == cancel){
				((JFrame)(((JButton)e.getSource()).getTopLevelAncestor())).dispose();
			}
			else if(bouton == confirmAndRegister){
				if(saveCarac()){
					if( ! tName.getText().equals("")){
						player.setTechList(playerListTech);
						player.setName(tName.getText());
						player.setGrade(gradeBox.getSelectedIndex());
						player.setWeaponList(playerListWeapon);
						player.setArmor(armorBox.getSelectedIndex());
						player.setAmorQuality(armorQualityBox.getSelectedIndex());
						player.setBonus(bonus.getText());
						//player.setKido(kido.isSelected());
						Utilitaires.savePlayer(player);
						playerPanel.setPlayer(player);
						((RefereeFrame)(playerPanel.getTopLevelAncestor())).refresh();
						((JFrame)(((JButton)e.getSource()).getTopLevelAncestor())).dispose();
					}
					else{
						JOptionPane.showMessageDialog(null, "veuillez entrer un nom avant d'enregistrer !", "Erreur septante : name not found", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
			else if(bouton == random){
				String result = JOptionPane.showInputDialog(null, "Veuillez entrer le niveau du PNJ", "G�n�rateur de PNJ", JOptionPane.QUESTION_MESSAGE);
				if(result != null){
					try{
						int niveau = Integer.parseInt(result);
						int carac[] = Utilitaires.randomCarac(niveau);
						for(int i = 0; i < textList.size() ; i++){
							textList.get(i).setText("" + carac[i]);
						}
					}
					catch(NumberFormatException nfe){
						JOptionPane.showMessageDialog(null, "Recommence avec un nombre pour voir si ca marche ?", "Erreur e^log(42)", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		}
		/*else{
			JCheckBox button = (JCheckBox)e.getSource();
			if(button == kido){
				textList.get(PlayerField.MP).setText("" + Integer.parseInt(textList.get(PlayerField.MP).getText())/2);
			}
		}*/
	}

	public void setWeaponBox(ArrayList<Weapon> weaponList){
		for(Weapon weapon : weaponList){
			weaponBox.addItem(weapon.getName());
			this.weaponList = weaponList;	
		}
	}

	public void setTechList(ArrayList<Technique> listTech){
		this.listTech = listTech;
		for(Technique technique : search(tSearch.getText())){
			dataModel.addElement(technique);
		}
	}

	public boolean saveCarac(){
		for(int i = 0; i < PlayerField.CARAC_LENGTH; i++){
			try{
				player.setCarac(i, Integer.parseInt(textList.get(i).getText()));
			}
			catch(NumberFormatException nfe){
				JOptionPane.showMessageDialog(null, "Tu n'avais qu'un job ! Entrer des nombres correctes !", "Erreur pie", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		return true;
	}

	public ArrayList<Technique> search(String text){
		ArrayList<Technique> findingTech = new ArrayList<Technique>();
		for(Technique tech : listTech){
			if(tech.getName().toLowerCase().contains(text.toLowerCase())){
				findingTech.add(tech);
			}
		}
		return findingTech;
	}

	public boolean notAgain(Technique tech){
		for(Technique newTech : playerListTech){
			if(newTech.getName().equals(tech.getName())){
				return false;
			}
		}
		return true;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	class PopClickListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e){
			JList list =(JList)e.getSource();
			if (e.getClickCount() == 2) {
				int index = list.locationToIndex(e.getPoint());
				if (index >= 0) {
					if(list == listWeapon){
						playerListWeapon.remove(list.getModel().getElementAt(index));
						((DefaultListModel)list.getModel()).removeElementAt(index);
					}
					else if(list == techList){
						playerListTech.remove((Technique)((DefaultListModel)list.getModel()).getElementAt(index));
						((DefaultListModel)list.getModel()).removeElementAt(index);
					}
					else if(list == dataBase && notAgain((Technique)((DefaultListModel)list.getModel()).getElementAt(index))){
						((DefaultListModel)techList.getModel()).addElement(((DefaultListModel)list.getModel()).getElementAt(index));
						//System.out.println(((Technique)((DefaultListModel)list.getModel()).getElementAt(index)).getText());
						playerListTech.add((Technique)((DefaultListModel)list.getModel()).getElementAt(index));
						//System.out.println(playerListTech.get(0).getText());
					}
				}
			}
			else if(e.getClickCount() == 10){
				bonus.setText("Hey !");
			}
			else if(e.getClickCount() == 15){
				bonus.setText("Stop it !");
			}
			else if(e.getClickCount() == 25){
				bonus.setText("what are you doing ?!");
			}
			else if(e.getClickCount() == 30){
				bonus.setText("I say STOP !");
			}
			else if(e.getClickCount() == 35){
				bonus.setText("I'm angry now, you should stop.");
			}
			else if(e.getClickCount() == 40){
				bonus.setText("1...");
			}
			else if(e.getClickCount() == 41){
				bonus.setText("2...");
			}
			else if(e.getClickCount() == 42){
				((JFrame)(((JList)e.getSource()).getTopLevelAncestor())).dispose();
				JOptionPane.showMessageDialog(null, "3 ! J'esp�re que tu es fier de toi ? (et que tu avais sauvegard� la fiche avant... mouhahahaha)", "Erreur 42 - click overflow", JOptionPane.ERROR_MESSAGE);
				bonus.setText("");
			}
		}
	}
	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if(key == KeyEvent.VK_ENTER && tSearch.getText().toLowerCase().equals("chuck norris")){
			((JFrame)(((JTextField)e.getSource()).getTopLevelAncestor())).dispose();
			JOptionPane.showMessageDialog(null, "On ne cherche pas Chuck Norris", "Erreur 0/infini ", JOptionPane.ERROR_MESSAGE);
		}
		else if (key == KeyEvent.VK_ENTER) { 
			dataModel.removeAllElements();
			for(Technique technique : search(tSearch.getText())){
				dataModel.addElement(technique);
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
}
