package component;

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import carac.PlayerField;
import carac.Weapon;

public class WeaponPanel extends JPanel{
	
	private JList list = new JList();
	private ArrayList<Weapon> weaponList = new ArrayList<Weapon>();
	private JLabel armor = new JLabel("Armure : ");
	DefaultListModel listModel = new DefaultListModel();
	
	public WeaponPanel(){
		super();
		this.setBorder(BorderFactory.createTitledBorder("liste d'armes"));
		
		for(Weapon weapon: getWeaponList()){
			listModel.addElement(weapon);
		}
		list.setModel(listModel);
		JScrollPane listScroller = new JScrollPane(list);
		listScroller.setPreferredSize(new Dimension(200, 50));
		//this.add(listScroller);
		
		
		JPanel top = new JPanel();
		JPanel bot = new JPanel();
		JPanel main = new JPanel();
		
		top.setLayout(new BoxLayout(top, BoxLayout.LINE_AXIS));
		bot.setLayout(new BoxLayout(bot, BoxLayout.LINE_AXIS));
		main.setLayout(new BoxLayout(main, BoxLayout.PAGE_AXIS));
		top.add(listScroller);
		bot.add(armor);
		main.add(top);
		main.add(bot);
		
		this.add(main);
	}

	public ArrayList<Weapon> getWeaponList() {
		return weaponList;
	}

	public void setWeaponList(ArrayList<Weapon> weaponList) {
		this.weaponList = weaponList;
		listModel.removeAllElements();
		for(Weapon weapon: getWeaponList()){
			listModel.addElement(weapon);
		}
	}
	
	public void setArmor(int armorCode, int qualityCode){
		armor.setText("Armure : " + PlayerField.getArmorName(armorCode) + " " + PlayerField.getQualityName(qualityCode));
	}
}
